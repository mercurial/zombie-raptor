;;;; Quaternions

;;; This file contains functional and destructive functions for
;;; quaternions. The functions with side effects are modeled on
;;; map-into, with the quaternion to overwite in the first position.
;;;
;;; To help make things very clear, the side effects functions follow
;;; the Scheme convention of "!" at the end.

(defpackage #:zombie-raptor/math/quaternion
  (:use #:cl)
  (:import-from #:sb-cga
                #:vec)
  (:import-from #:zombie-raptor/math/math
                #:+single-pi+)
  (:export #:quaternion
           #:invert-quaternion
           #:quaternion*
           #:quaternion-dot
           #:quaternion*-into
           #:quaternion+
           #:quaternion+-into
           #:quaternion-
           #:quaternion--into
           #:scalar*quaternion
           #:scalar*quaternion-into
           #:rotation-quaternion
           #:quaternion-rotation-of-vector
           #:quaternion->rotation-matrix
           #:slerp
           #:rotate-yaw
           #:rotate-pitch
           #:rotate-roll))

(in-package #:zombie-raptor/math/quaternion)

(deftype quaternion ()
  '(simple-array single-float (4)))

(defun quaternion (w x y z)
  (make-array 4 :element-type 'single-float :initial-contents (list w x y z)))

(defun invert-quaternion (q)
  (quaternion (elt q 0) (- (elt q 1)) (- (elt q 2)) (- (elt q 3))))

;;; todo: invert-quaternion-into

(defun quaternion* (q1 q2)
  (let ((w1 (elt q1 0))
        (w2 (elt q2 0))
        (x1 (elt q1 1))
        (x2 (elt q2 1))
        (y1 (elt q1 2))
        (y2 (elt q2 2))
        (z1 (elt q1 3))
        (z2 (elt q2 3)))
    (quaternion (- (* w1 w2) (* x1 x2) (* y1 y2) (* z1 z2))
                (+ (* w1 x2) (* x1 w2) (* y1 z2) (- (* z1 y2)))
                (+ (* w1 y2) (- (* x1 z2)) (* y1 w2) (* z1 x2))
                (+ (* w1 z2) (* x1 y2) (- (* y1 x2)) (* z1 w2)))))

(defun quaternion*-into (result-q q1 q2)
  (let ((w1 (elt q1 0))
        (w2 (elt q2 0))
        (x1 (elt q1 1))
        (x2 (elt q2 1))
        (y1 (elt q1 2))
        (y2 (elt q2 2))
        (z1 (elt q1 3))
        (z2 (elt q2 3)))
    (setf (elt result-q 0) (- (* w1 w2) (* x1 x2) (* y1 y2) (* z1 z2))
          (elt result-q 1) (+ (* w1 x2) (* x1 w2) (* y1 z2) (- (* z1 y2)))
          (elt result-q 2) (+ (* w1 y2) (- (* x1 z2)) (* y1 w2) (* z1 x2))
          (elt result-q 3) (+ (* w1 z2) (* x1 y2) (- (* y1 x2)) (* z1 w2)))))

(defun quaternion-dot (q1 q2)
  (reduce #'+ (map 'vector #'* q1 q2)))

;;; todo: quaternion-dot-into

(defun quaternion+ (q1 q2)
  (map 'quaternion #'+ q1 q2))

(defun quaternion+-into! (result-q q1 q2)
  (map-into result-q #'+ q1 q2))

(defun quaternion- (q1 q2)
  (map 'quaternion #'- q1 q2))

(defun quaternion--into! (result-q q1 q2)
  (map-into result-q #'- q1 q2))

(defun scalar*quaternion (a q)
  (map 'quaternion
       (lambda (b)
         (* a b))
       q))

(defun scalar*quaternion-into! (result-q a q)
  (map-into result-q
            (lambda (b)
              (* a b))
            q))

(defun vector-to-quaternion (v)
  (quaternion 0.0 (elt v 0) (elt v 1) (elt v 2)))

(defun quaternion-to-vector (q)
  (vec (elt q 1) (elt q 2) (elt q 3)))

(defun rotation-quaternion (u-x u-y u-z theta)
  (quaternion (cos (* 0.5 theta))
              (* (sin (* 0.5 theta)) u-x)
              (* (sin (* 0.5 theta)) u-y)
              (* (sin (* 0.5 theta)) u-z)))

(defun quaternion-rotation-of-vector (q v)
  (quaternion-to-vector (quaternion* (quaternion* q (vector-to-quaternion v))
                                     (quaternion (elt q 0) (- (elt q 1)) (- (elt q 2)) (- (elt q 3))))))

(defun slerp (q1 q2 time)
  (declare (ignore q1 q2 time))
  "Stub")

(defun rotate-yaw (angle)
  (rotation-quaternion 0f0 1f0 0f0 angle))

(defun rotate-pitch (angle)
  (rotation-quaternion 1f0 0f0 0f0 angle))

(defun rotate-roll (angle)
  (rotation-quaternion 0f0 0f0 1f0 angle))

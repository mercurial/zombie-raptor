;;;; Math functions

;;;; If something doesn't have enough math functions for its own file,
;;;; they go here.

(defpackage #:zombie-raptor/math/math
  (:use #:cl)
  (:import-from #:alexandria
                #:lerp)
  (:import-from #:sb-cga
                #:vec)
  (:export #:+single-pi+
           #:vec-lerp))

(in-package #:zombie-raptor/math/math)

(defconstant +single-pi+ (coerce pi 'single-float))

(defun vec-lerp (vec1 vec2 time)
  (map 'vec
       (lambda (a b)
         (lerp time a b))
       vec1
       vec2))

;;;; Matrices

(defpackage #:zombie-raptor/math/matrix
  (:use #:cl)
  (:import-from #:sb-cga
                #:inverse-matrix
                #:matrix
                #:matrix*
                #:scale
                #:translate
                #:transpose-matrix
                #:vec)
  (:import-from #:zombie-raptor/math/math
                #:+single-pi+
                #:vec-lerp)
  (:import-from #:zombie-raptor/math/quaternion
                #:invert-quaternion
                #:quaternion*)
  (:export #:perspective-matrix
           #:finite-frustum
           #:infinite-frustum
           #:ortho
           #:make-ortho
           #:make-projection-matrix
           #:camera-matrix
           #:camera-2d-matrix
           #:model-matrix
           #:normal-matrix))

(in-package #:zombie-raptor/math/matrix)

(defun perspective-matrix (fovy aspect znear zfar)
  "Uses an implementation of the gluPerspective matrix. Requires
single-float numbers.

  FOVY is the vertical field of view, in degrees.

  ASPECT is the aspect ratio of the window, width / height.

  Z-NEAR and Z-FAR are positive numbers representing the depth
  clipping planes."
  (let ((f (/ (tan (* fovy (/ +single-pi+ 360f0))))))
    (matrix (/ f aspect) 0f0 0f0 0f0
            0f0 f 0f0 0f0
            0f0 0f0 (/ (+ zfar znear) (- znear zfar)) (/ (* 2f0 zfar znear) (- znear zfar))
            0f0 0f0 -1f0 0f0)))

(defun finite-frustum (left right bottom top near far)
  (matrix (/ (* 2f0 near) (- right left)) 0f0 (/ (+ right left) (- right left)) 0f0
          0f0 (/ (* 2f0 near) (- top bottom)) (/ (+ top bottom) (- top bottom)) 0f0
          0f0 0f0 (- (/ (+ far near) (- far near))) (- (/ (* 2 far near) (- far near)))
          0f0 0f0 -1f0 0f0))

(defun infinite-frustum (left right bottom top near)
  (matrix (/ (* 2f0 near) (- right left)) 0f0 (/ (+ right left) (- right left)) 0f0
          0f0 (/ (* 2f0 near) (- top bottom)) (/ (+ top bottom) (- top bottom)) 0f0
          0f0 0f0 -1f0 (* -2f0 near)
          0f0 0f0 -1f0 0f0))

(defun ortho (left right bottom top near far)
  (matrix (/ 2f0 (- right left)) 0f0 0f0 (- (/ (+ right left) (- right left)))
          0f0 (/ 2f0 (- top bottom)) 0f0 (- (/ (+ top bottom) (- top bottom)))
          0f0 0f0 (/ -2f0 (- far near)) (- (/ (+ far near) (- far near)))
          0f0 0f0 0f0 1f0))

;; fixme: The matrix uses fovy. The fov everyone talks in is
;; fovx. This should work in fovx instead of fovy.
(defun make-projection-matrix (fovy aspect-ratio)
  (let* ((near 0.1f0)
         (top (* (tan (* fovy +single-pi+ (/ 360f0))) near))
         (right (* aspect-ratio top)))
    ;; (infinite-frustum (- right) right (- top) top near)
    (finite-frustum (- right) right (- top) top near 500f0))
  ;; (perspective-matrix fovy aspect-ratio 1f0 500.0)
  )

(defun make-ortho (width height)
  (ortho (- (* 0.5 width)) (* 0.5 width) (- (* 0.5 height)) (* 0.5 height) 0.1f0 10f0))

(defun quaternion->rotation-matrix (q)
  (let ((w (elt q 0))
        (i (elt q 1))
        (j (elt q 2))
        (k (elt q 3)))
    (matrix (- 1 (* 2 (expt j 2)) (* 2 (expt k 2)))
            (* 2 (- (* i j) (* k w)))
            (* 2 (+ (* i k) (* j w)))
            0f0
            (* 2 (+ (* i j) (* k w)))
            (- 1 (* 2 (expt i 2)) (* 2 (expt k 2)))
            (* 2 (- (* j k) (* i w)))
            0f0
            (* 2 (- (* i k) (* j w)))
            (* 2 (+ (* j k) (* i w)))
            (- 1 (* 2 (expt i 2)) (* 2 (expt j 2)))
            0f0
            0f0 0f0 0f0 1f0)))

(defun camera-matrix (old-location new-location rotations time)
  (matrix* (quaternion->rotation-matrix (quaternion* (invert-quaternion (elt rotations 1))
                                                     (invert-quaternion (elt rotations 0))))
           (translate (vec-lerp (sb-cga:vec* old-location -1f0)
                                (sb-cga:vec* new-location -1f0)
                                time))))

(defun camera-2d-matrix ()
  (translate (vec 0f0 0f0 -1f0)))

(defun model-matrix (old-location new-location rotation scale time)
  (matrix* (translate (vec-lerp old-location new-location time))
           (quaternion->rotation-matrix (elt rotation 0)) ; (slerp (elt rotation 2) (elt rotation 0) time)
           (scale scale)))

(defun normal-matrix (model-matrix)
  (transpose-matrix (inverse-matrix model-matrix)))

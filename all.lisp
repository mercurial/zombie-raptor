;;;; This file is used to reexport all of the individual packages into
;;;; a more convenient zombie-raptor package because of the way
;;;; package-inferred-system works.

(uiop:define-package #:zombie-raptor/all
  (:nicknames #:zombie-raptor #:zr)
  (:use-reexport #:zombie-raptor/audio/sdl2-mixer
                 #:zombie-raptor/core/controls
                 #:zombie-raptor/core/example
                 #:zombie-raptor/core/input
                 #:zombie-raptor/core/sdl2
                 #:zombie-raptor/core/window
                 #:zombie-raptor/data/data-path
                 #:zombie-raptor/data/game-data
                 #:zombie-raptor/data/model
                 #:zombie-raptor/data/shader
                 #:zombie-raptor/entity/entity
                 #:zombie-raptor/entity/physics
                 #:zombie-raptor/entity/render
                 #:zombie-raptor/math/math
                 #:zombie-raptor/math/matrix
                 #:zombie-raptor/math/quaternion
                 #:zombie-raptor/physics/physics
                 #:zombie-raptor/render/gl-data
                 #:zombie-raptor/render/gl))

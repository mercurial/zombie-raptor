Zombie Raptor
=============

A 3D game engine written in Common Lisp, built on OpenGL and SDL

Warning
-------

This repository is currently a work-in-progress. Anything could and
probably will change at any moment.

Installation
------------

See [INSTALL.md](INSTALL.md) for installation instructions.

Outline
-------

See [OUTLINE.md](OUTLINE.md) for an outline of files and directories.

Contributing
------------
See [CONTRIBUTING.md](CONTRIBUTING.md) for guidelines.

License
-------

MIT (Expat) License. See [LICENSE.txt](LICENSE.txt) for the license text.

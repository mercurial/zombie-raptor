;;;; Rendering System

(defpackage #:zombie-raptor/entity/render
  (:use #:cl
        #:zombie-raptor/entity/entity
        #:zombie-raptor/render/gl)
  (:import-from #:zombie-raptor/math/matrix
                #:camera-matrix
                #:camera-2d-matrix
                #:model-matrix
                #:normal-matrix)
  (:export #:render-2d-hud
           #:render-entities))

(in-package #:zombie-raptor/entity/render)

(defun render-2d-hud (ecs meshes program hud-matrix time)
  (with-shader-program (program)
    (uniform-matrix program :projection-matrix hud-matrix)
    (uniform-matrix program :view-matrix (camera-2d-matrix))
    (do-with-system (ecs +render-system+ (location rotation scale geometry))
      (when (geometry-component-renderable geometry)
        (uniform-matrix program :model-matrix (model-matrix (locations-old-location location)
                                                            (locations-location location)
                                                            rotation
                                                            scale
                                                            time))
        ;; instead of hardcoded id use (texture textures geometry)
        (draw (mesh meshes geometry) 1 program)))))

(defun render-entities (ecs meshes program perspective-matrix camera-id time)
  "The render system of the ECS."
  (with-shader-program (program)
    (uniform-matrix program :projection-matrix perspective-matrix)
    (let* ((camera-locations (entity-component ecs camera-id :location))
           (old-camera-location (locations-old-location camera-locations))
           (camera-location (locations-location camera-locations)))
      (uniform-matrix program :view-matrix (camera-matrix old-camera-location
                                                          camera-location
                                                          (entity-component ecs camera-id :rotation)
                                                          time)))
    (do-with-system (ecs +render-system+ (location rotation scale geometry))
      (when (geometry-component-renderable geometry)
        (let ((model-matrix (model-matrix (locations-old-location location)
                                          (locations-location location)
                                          rotation
                                          scale
                                          time)))
          (uniform-matrix program :model-matrix model-matrix)
          (uniform-matrix program :normal-matrix (normal-matrix model-matrix)))
        ;; instead of hardcoded id use (texture textures geometry)
        (draw (mesh meshes geometry) 1 program)))))

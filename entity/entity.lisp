;;;; Entity Component System

(defpackage #:zombie-raptor/entity/entity
  (:use #:cl
        #:zombie-raptor/math/quaternion)
  (:import-from #:alexandria #:make-gensym-list)
  (:import-from #:sb-cga
                #:transform-direction
                #:vec)
  (:import-from #:zombie-raptor/math/math
                #:+single-pi+)
  (:export #:entity-component-system
           #:make-entity-component-system
           #:make-basic-entity
           #:make-fps-camera-entity
           #:entity-component
           #:add-entity
           #:add-component
           #:delete-component
           #:delete-entity
           #:location
           #:velocity
           #:rotation
           #:geometry
           #:move-entity
           #:rotate-entity-yaw
           #:rotate-entity-pitch
           #:rotate-entity-roll
           #:rotate-entity-camera-pitch
           #:change-shape
           #:+render-system+
           #:+physics-system+
           #:do-with-system
           #:physics-acceleration
           #:physics-gravityp
           #:physics-jumps
           #:store-old-states
           #:locations-location
           #:locations-old-location
           #:mesh
           #:geometry-component-renderable))

(in-package #:zombie-raptor/entity/entity)

;;;; This file sets up an entity component system. It stores the
;;;; component data in `entity-component-system'. There is no entity
;;;; class. It is just an ID.
;;;;
;;;; The ECS itself should be used only through the exported functions
;;;; and macros. A new ECS is created and returned with the function
;;;; `make-entity-component-system'. Entities are created with
;;;; `add-entity'. Components associated with entity-ids are managed
;;;; with `add-component' and `delete-component'.
;;;;
;;;; The ECS is iterated using `do-with-system'.

(defconstant +max-entities+ 2500
  "This is the number of entities that can be created")

(defconstant +physics+  #b100000)
(defconstant +location+ #b010000)
(defconstant +velocity+ #b001000)
(defconstant +geometry+ #b000100)
(defconstant +rotation+ #b000010)
(defconstant +scale+    #b000001)

(defconstant +no-system+ #b000000)
(defconstant +render-system+  (logior +location+ +rotation+ +scale+ +geometry+))
(defconstant +physics-system+ (logior +location+ +velocity+ +physics+))

;;; sqrt(2) / 2 coerced ahead of time to single-float
(defconstant +half-sqrt2+ (coerce (* 0.5 (sqrt 2)) 'single-float))

(defclass entity-component-system ()
  ((max-id
    :accessor max-id
    :initform -1)
   (entity
    :reader entity
    :initform (make-array +max-entities+ :element-type '(unsigned-byte 6) :initial-element 0))
   (location
    :reader location
    :initform (make-hash-table))
   (velocity
    :reader velocity
    :initform (make-hash-table))
   (rotation
    :reader rotation
    :initform (make-hash-table))
   (geometry
    :reader geometry
    :initform (make-hash-table))
   (scale
    :reader scale
    :initform (make-hash-table))
   (physics
    :reader physics
    :initform (make-hash-table))))

(declaim (inline has-components))
(defun has-components (component-bits entity)
  (= component-bits (logand component-bits entity)))

(defun make-component (component-name)
  (ecase component-name
    (:location #'make-location-vectors)
    (:velocity #'make-vec)
    (:rotation #'make-rotation-component)
    (:geometry #'make-geometry-component)
    (:physics  #'make-physics)
    (:scale    #'make-scale)))

(defun component-constant (component-keyword)
  (ecase component-keyword
    (:location +location+)
    (:velocity +velocity+)
    (:rotation +rotation+)
    (:geometry +geometry+)
    (:physics  +physics+)
    (:scale    +scale+)))

(defun component-ecs-table (ecs component-keyword)
  (ecase component-keyword
    (:location (location ecs))
    (:velocity (velocity ecs))
    (:rotation (rotation ecs))
    (:geometry (geometry ecs))
    (:physics  (physics ecs))
    (:scale    (scale ecs))))

(defun entity-component (ecs entity-id component-type)
  (gethash entity-id (component-ecs-table ecs component-type)))

(defun (setf entity-component) (new-value ecs entity-id component-type)
  (setf (gethash entity-id (component-ecs-table ecs component-type)) new-value))

(eval-when (:compile-toplevel :execute :load-toplevel)
  (defun component-symbols-to-component-arrays (component-symbols gensyms ecs)
    (mapcar (lambda (gensym component-symbol)
              (list gensym
                    (list 'component-ecs-table
                          ecs
                          (intern (symbol-name component-symbol) :keyword))))
            gensyms
            component-symbols))

  (defun bind-components (component-symbols gensyms i)
    (mapcar (lambda (component-symbol gensym)
              (list component-symbol `(gethash ,i ,gensym)))
            component-symbols
            gensyms)))

(defmacro do-with-system ((ecs system component-names) &body body)
  "Takes an ECS, a system, and a list of component names, and then
iterates over every entity that implements all of the components that
the system requires, binding components to the component names."
  (let ((i (gensym))
        (gensyms (make-gensym-list (length component-names) "COMPONENT-ARRAY")))
    `(let ,(component-symbols-to-component-arrays component-names gensyms ecs)
       (dotimes (,i (1+ (max-id ,ecs)))
         (when (has-components ,system (aref (entity ,ecs) ,i))
           (let ,(bind-components component-names gensyms i)
             ,@body))))))

(defun make-entity-component-system ()
  (make-instance 'entity-component-system))

(defun add-entity (ecs)
  "Adds a new entity to the ECS."
  (check-type ecs entity-component-system)
  ;; todo: bounds check and recycle entity IDs
  (incf (max-id ecs)))

(defun add-component (ecs entity-id component-name &rest initargs)
  "Creates and stores a component object of type component-name with
optional initargs, associated with entity-id."
  (check-type ecs entity-component-system)
  (check-type entity-id integer)
  (check-type component-name keyword)
  (setf (aref (entity ecs) entity-id)
        (logior (component-constant component-name)
                (aref (entity ecs) entity-id)))
  (setf (entity-component ecs entity-id component-name)
        (apply (make-component component-name) initargs)))

(defun delete-component (ecs entity-id component-name)
  "Deletes a component object of type component-name, associated with
entity-id."
  (check-type ecs entity-component-system)
  (check-type entity-id integer)
  (check-type component-name keyword)
  (when (has-components +location+ (aref (entity ecs) entity-id))
    (setf (aref (entity ecs) entity-id)
          (logxor (component-constant component-name)
                  (aref (entity ecs) entity-id))))
  (remhash entity-id (component-ecs-table ecs component-name)))

(defun delete-entity (ecs entity-id)
  "Deletes all components from an entity."
  (check-type ecs entity-component-system)
  (check-type entity-id integer)
  (let ((entity (aref (entity ecs) entity-id)))
    (when (has-components +location+ entity)
      (remhash entity-id (location ecs)))
    (when (has-components +velocity+ entity)
      (remhash entity-id (velocity ecs)))
    (when (has-components +rotation+ entity)
      (remhash entity-id (rotation ecs)))
    (when (has-components +geometry+ entity)
      (remhash entity-id (geometry ecs)))
    (when (has-components +physics+ entity)
      (remhash entity-id (physics ecs)))
    (when (has-components +scale+ entity)
      (remhash entity-id (scale ecs))))
  (setf (aref (entity ecs) entity-id) +no-system+))

(defun make-basic-entity (ecs mesh &key location velocity acceleration rotation scale)
  (let ((entity-id (add-entity ecs)))
    (add-component ecs entity-id :geometry :mesh-key mesh)
    (if location
        (add-component ecs entity-id :location :x (elt location 0) :y (elt location 1) :z (elt location 2))
        (add-component ecs entity-id :location))
    (if velocity
        (add-component ecs entity-id :velocity :x (elt velocity 0) :y (elt velocity 1) :z (elt velocity 2))
        (add-component ecs entity-id :velocity))
    (if rotation
        (add-component ecs entity-id :rotation :yaw (elt rotation 0) :pitch (elt rotation 1) :roll (elt rotation 2))
        (add-component ecs entity-id :rotation))
    (if scale
        (add-component ecs entity-id :scale :x (elt scale 0) :y (elt scale 1) :z (elt scale 2))
        (add-component ecs entity-id :scale))
    (if acceleration
        (add-component ecs entity-id :physics :acceleration acceleration)
        (add-component ecs entity-id :physics))
    entity-id))

(defun make-fps-camera-entity (ecs &key location rotation)
  (let ((camera-id (add-entity ecs)))
    (if location
        (add-component ecs camera-id :location :x (elt location 0) :y (elt location 1) :z (elt location 2))
        (add-component ecs camera-id :location))
    (if rotation
        (add-component ecs camera-id :rotation :yaw (elt rotation 0) :pitch (elt rotation 1) :roll (elt rotation 2))
        (add-component ecs camera-id :rotation))
    (add-component ecs camera-id :velocity)
    (add-component ecs camera-id :physics)
    camera-id))

;;;; Components

(defstruct locations
  (old-location (vec 0f0 0f0 0f0) :type vec)
  (location (vec 0f0 0f0 0f0) :type vec))

(defun make-location-vectors (&key (x 0f0) (y 0f0) (z 0f0))
  (make-locations :old-location (vec x y z)
                  :location (vec x y z)))

(defstruct rotations
  (old-rotation (quaternion 0f0 0f0 0f0 0f0) :type quaternion)
  (rotation (quaternion 0f0 0f0 0f0 0f0) :type quaternion))

(defun make-rotation-quaternions (&key (w 0f0) (x 0f0) (y 0f0) (z 0f0))
  (make-rotations :old-rotation (quaternion w x y z)
                  :rotation (quaternion w x y z)))

(defun make-vec (&key (x 0f0) (y 0f0) (z 0f0))
  (vec x y z))

(defun make-scale (&key (x 1f0) (y 1f0) (z 1f0))
  (vec x y z))

(defun store-old-states (ecs)
  (do-with-system (ecs +location+ (location))
    (replace (locations-old-location location) (locations-location location)))
  (do-with-system (ecs +rotation+ (rotation))
    (replace (elt rotation 2) (elt rotation 0))
    (replace (elt rotation 3) (elt rotation 1))))

;;; a list of four quaternions: the entity's quaternion, the optional
;;; camera look quaternion, and the old versions of both for slerping
;;;
;;; todo: this is only set up this way temporarily
(defun make-rotation-component (&key (yaw 0f0) (pitch 0f0) (roll 0f0))
  (list (quaternion* (rotate-yaw yaw) (quaternion* (rotate-pitch pitch) (rotate-roll roll)))
        (rotate-pitch 0f0)
        (quaternion* (rotate-yaw yaw) (quaternion* (rotate-pitch pitch) (rotate-roll roll)))
        (rotate-pitch 0f0)))

(defstruct geometry-component
  (mesh-key :cube :type keyword)
  (texture-key :foo :type keyword)
  (renderable t :type boolean))

(defun mesh (meshes geometry-component)
  (gethash (geometry-component-mesh-key geometry-component) meshes))

(defun texture (textures geometry-component)
  (gethash (geometry-component-texture-key geometry-component) textures))

(defstruct physics
  (acceleration (vec 0f0 0f0 0f0) :type vec)
  (gravityp nil :type boolean)
  (jumps 0 :type (unsigned-byte 8)))

;;;; Actions on entities

(defun move-entity (ecs entity-id distance-vector)
  (let ((location (locations-location (entity-component ecs entity-id :location))))
    (map-into location
              #'+
              location
              (quaternion-rotation-of-vector (elt (entity-component ecs entity-id :rotation) 0)
                                             distance-vector))))

(defun entity-jump (ecs entity-id quantity)
  (let ((physics (entity-component ecs entity-id :physics)))
    ;; Single jump
    (when (and (zerop (physics-jumps physics))
               (not (physics-gravityp physics)))
      (setf (physics-jumps physics) 1)
      (incf (elt (entity-component ecs entity-id :velocity) 1) quantity))
    ;; ;; Double jump
    ;; (when (and (physics-gravityp physics)
    ;;            (or (zerop (physics-jumps physics))
    ;;                (<= 25 (physics-jumps physics))))
    ;;   (incf (elt (entity-component ecs entity-id :velocity) 1) quantity)
    ;;   (setf (physics-jumps physics) 2))
    ))

(defun rotate-entity-yaw (ecs entity-id diff)
  (quaternion*-into (elt (entity-component ecs entity-id :rotation) 0)
                    (elt (entity-component ecs entity-id :rotation) 0)
                    (rotate-yaw diff)))

(defun rotate-entity-pitch (ecs entity-id diff)
  (quaternion*-into (elt (entity-component ecs entity-id :rotation) 0)
                    (elt (entity-component ecs entity-id :rotation) 0)
                    (rotate-pitch diff)))

(defun rotate-entity-roll (ecs entity-id diff)
  (quaternion*-into (elt (entity-component ecs entity-id :rotation) 0)
                    (elt (entity-component ecs entity-id :rotation) 0)
                    (rotate-roll diff)))

(defun rotate-entity-camera-pitch (ecs entity-id diff)
  (let* ((rotation-quat (elt (entity-component ecs entity-id :rotation) 1)))
    (if (<= (- +half-sqrt2+) (elt (quaternion* (rotate-pitch diff) rotation-quat) 1) +half-sqrt2+)
        (quaternion*-into rotation-quat rotation-quat (rotate-pitch diff)))))

(defun change-shape (ecs entity-id new-shape)
  (setf (geometry-component-mesh-key (entity-component ecs entity-id :geometry)) new-shape))

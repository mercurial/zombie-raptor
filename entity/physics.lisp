;;;; Physics System

(defpackage #:zombie-raptor/entity/physics
  (:use #:cl
        #:zombie-raptor/entity/entity)
  (:import-from #:zombie-raptor/physics/physics
                #:acceleration
                #:update-location-and-velocity!)
  (:export #:physics))

(in-package #:zombie-raptor/entity/physics)

(defparameter *gravity* -9.8f0)

(defun apply-gravity-force (acceleration)
  (incf (elt acceleration 1) *gravity*))

(defun remove-gravity-force (acceleration)
  (decf (elt acceleration 1) *gravity*))

(defun physics (ecs time time-step)
  "The physics system of the ECS."
  (do-with-system (ecs +physics-system+ (location velocity physics))
    (let ((l (locations-location location))
          (v velocity)
          (a (physics-acceleration physics))
          (entity-height-offset 1.75f0) ; todo: determine this per-geometry
          (ground-level 0f0) ; todo: don't hardcode this
          (epsilon 0.01f0))
      (when (and (not (physics-gravityp physics)) (> (elt l 1) (+ ground-level entity-height-offset epsilon)))
        (apply-gravity-force a)
        (setf (physics-gravityp physics) t))
      (when (and (physics-gravityp physics) (< (elt l 1) (+ ground-level entity-height-offset epsilon)))
        (remove-gravity-force a)
        (setf (elt v 1) 0f0)
        (setf (elt l 1) (+ ground-level entity-height-offset))
        (setf (physics-gravityp physics) nil)
        (setf (physics-jumps physics) 0))
      (when (and (physics-gravityp physics) (not (zerop (physics-jumps physics))))
        (incf (physics-jumps physics)))
      (update-location-and-velocity! l v a time time-step #'acceleration))))

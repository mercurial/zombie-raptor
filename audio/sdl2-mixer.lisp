(defpackage #:zombie-raptor/audio/sdl2-mixer
  (:use #:cl #:sdl2-mixer)
  (:import-from #:sdl2)
  (:export #:setup-sdl2-mixer
           #:exit-sdl2-mixer))

(in-package #:zombie-raptor/audio/sdl2-mixer)

(defun setup-sdl-mixer ()
  (sdl2-mixer:init :ogg)
  ;; (sdl2-mixer:open-audio )
  )

(defun exit-sdl2-mixer ()
  (sdl2-mixer:halt-music)
  (sdl2-mixer:close-audio)
  (sdl2-mixer:quit))

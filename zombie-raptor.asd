;;; Requires an ASDF version with package-inferred-system
(unless (asdf:version-satisfies (asdf:asdf-version) "3.1.2")
  (error "Zombie Raptor requires ASDF 3.1.2 or later."))

(asdf:defsystem #:zombie-raptor
  :description "A game engine written in Common Lisp"
  :version "0.0.0.0"
  :author "Michael Babich"
  :license "MIT"
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :depends-on (:zombie-raptor/all))

;;; All of the dependencies where the package name doesn't match the
;;; ASDF system name need to be registered
(asdf:register-system-packages :rutils '(:rutils :rutils.readtable))
(asdf:register-system-packages :cl-opengl '(:cl-opengl :gl :%gl))
(asdf:register-system-packages :sdl2 '(:sdl2 :sdl2-ffi))
(asdf:register-system-packages :cl-plus-c '(:plus-c))

#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 color;
layout(location = 2) in vec2 texcoord;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
out vec3 fragPosition;
out vec3 color2;
out vec2 textureCoordinate;

void main()
{
  color2 = color;
  textureCoordinate = texcoord;
  fragPosition = vec3(modelMatrix * vec4(position, 1.0f));
  gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0f);
}

#version 330 core

in vec3 vertexNormalInterpolation;
in vec3 fragPosition;
in vec2 textureCoordinate;
out vec4 outColor;

uniform sampler2D loadedTexture;

void main()
{
  vec3 surfaceNormal = normalize(vertexNormalInterpolation);
  vec3 objectColor = vec3(texture(loadedTexture, textureCoordinate)) * abs(surfaceNormal); // todo: this is for debugging models and lighting
  float ambientStrength = 0.7f; // todo: doesn't need to be hardcoded
  vec3 ambientColor = vec3(1.0f, 1.0f, 1.0f); // todo: doesn't need to be hardcoded
  vec3 ambientLight = ambientStrength * ambientColor;
  outColor = vec4(ambientLight * objectColor, 1.0f);
}

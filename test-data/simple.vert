#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texcoord;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
uniform mat4 normalMatrix;
out vec3 fragPosition;
out vec3 vertexNormalInterpolation;
out vec2 textureCoordinate;

void main()
{
  fragPosition = vec3(modelMatrix * vec4(position, 1.0f));
  // It becomes the interpolation when passed to the fragment shader.
  vertexNormalInterpolation = mat3(normalMatrix) * normal;
  textureCoordinate = texcoord;
  gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0f);
}

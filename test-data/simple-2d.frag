#version 330 core

in vec3 color2;
in vec3 fragPosition;
in vec2 textureCoordinate;
out vec4 outColor;

uniform sampler2D loadedTexture;

void main()
{
  vec3 objectColor = color2; // vec3(texture(loadedTexture, textureCoordinate)) * color2;
  outColor = vec4(objectColor, 1.0f);
}

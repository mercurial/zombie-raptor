(defpackage #:zombie-raptor/data/data-path
  (:use #:cl #:uiop/pathname #:uiop/configuration #:uiop/filesystem #:uiop/os)
  (:export #:local-data-path
           #:local-config-path
           #:local-cache-path
           #:main-data-directory))

(in-package #:zombie-raptor/data/data-path)

;; (defclass data-paths ()
;;   ((app-name
;;     :initarg :app-name
;;     :accessor app-name
;;     :initform (error "An application name is required.")
;;     :documentation "The app name as it appears in the directory
;;     path(s) that store app data.")
;;    (org-name
;;     :initarg :org-name
;;     :accessor org-name
;;     :initform nil
;;     :documentation "An optional organization name that sometimes is
;;     the parent for app names in directory paths.")
;;    (app-data
;;     :initarg :app-data-path
;;     :accessor app-data-path
;;     :initform nil
;;     :documentation "The path to app data that comes with the app,
;;     which should be treated as immutable data.")
;;    (local-data
;;     :accessor local-data-path
;;     :documentation "The path to app data that is stored by the app in
;;     the user's home directory.")
;;    (local-config
;;     :accessor local-config-path
;;     :documentation "The path to configuration data that is stored by
;;     the app in the user's home directory.")
;;    (local-cache-path
;;     :accessor local-cache-path
;;     :documentation "The path to user-specific cache data, which is cached to speed up the application."))
;;   (:documentation "Stores various data paths that the application
;;   needs to know so that they only need to be computed once."))

;; (defmethod initialize-instance :after ((data-paths data-paths) &key)
;;   (with-accessors ((app-name app-name)
;;                    (org-name org-name)
;;                    (local-data local-data-path)
;;                    (local-config local-config-path)
;;                    (cache local-cache-path))
;;       data-paths
;;     (setf local-data (local-data-path app-name org-name)
;;           local-config (local-config-path app-name org-name)
;;           cache (local-cache-path app-name org-name))))

(defun path-from-dir (dir &rest path-strings)
  "Generates an absolute pathname object from directories as strings
and makes it come after the given directory, dir."
  (uiop/pathname:merge-pathnames* (uiop/pathname:make-pathname* :directory `(:relative ,@path-strings)) dir))

(defun path-from-root (&rest path-strings)
  "Generates an absolute pathname object from directories as strings,
in order."
  (uiop/pathname:make-pathname* :directory `(:absolute ,@path-strings)))

(defun app-directory-path (lookup-function org-name app-name)
  "Generates a full path to an application's directory given a
lookup-function that provides the base path, an app-name directory,
and an optional org-name directory."
  (apply #'path-from-dir (funcall lookup-function) (append (if org-name (list org-name)) (list app-name))))

(defun local-data-path (app-name &optional org-name)
  "Gets the OS-specific idiomatic directory location for the local,
user-specific data directory."
  (app-directory-path #'uiop/configuration:xdg-data-home org-name app-name))

(defun local-config-path (app-name &optional org-name)
  "Gets the OS-specific idiomatic directory location for the local,
user-specific configuration files if it exists, or places it in the
general app data directory if it doesn't."
  (if (uiop/os:os-windows-p)
      (path-from-dir (local-data-path app-name org-name) "config")
      (app-directory-path #'uiop/configuration:xdg-config-home org-name app-name)))

(defun local-cache-path (app-name &optional org-name)
  "Gets the OS-specific idiomatic directory location for the local,
user-specific cache files if it exists, or places it in the general
app data directory if it doesn't."
  (if (uiop/os:os-windows-p)
      (path-from-dir (local-data-path app-name org-name) "cache")
      (app-directory-path #'uiop/configuration:xdg-cache-home org-name app-name)))

(defun main-data-directory (system-name &rest path-strings)
  "Gets the main (non-local) data directory for the app."
  (apply #'path-from-dir (asdf:system-source-directory system-name) path-strings))

(defpackage #:zombie-raptor/data/model
  (:use #:cl)
  (:import-from #:alexandria
                #:doplist
                #:non-negative-fixnum
                #:positive-fixnum)
  (:import-from #:sb-cga
                #:cross-product
                #:normalize
                #:vec
                #:vec-)
  (:export #:+vertex-size+
           #:+normal-size+
           #:+texcoord-size+
           #:+stride+
           #:model
           #:models
           #:vertex-array
           #:element-array
           #:make-hexagon
           #:make-square
           #:make-cube
           #:make-cuboid
           #:make-level
           #:make-platforming-level
           #:texture
           #:texture-data
           #:generate-textures))

(in-package #:zombie-raptor/data/model)

;;; Only the value at the start of the program matters. Set to nil to
;;; generate parts of the walls that can't possibly be seen from the
;;; inside of the level.
(defparameter *remove-hidden-wall-surfaces* t)

(defconstant +vertex-size+ 3)
(defconstant +normal-size+ 3)
(defconstant +texcoord-size+ 2)
(defconstant +stride+ (+ +vertex-size+ +normal-size+ +texcoord-size+))

(defclass model ()
  ((vertex-array :accessor vertex-array)
   (element-array :accessor element-array)
   (attributes :accessor attributes)
   (stride :accessor stride)
   (vertex-count :accessor vertex-count)
   (element-count :accessor element-count)
   (vertex-counter :initform 0
                   :accessor vertex-counter)
   (element-number-counter :initform 0
                           :accessor element-number-counter)
   (element-place-counter :initform 0
                          :accessor element-place-counter)))

;;;; Generates model

;;; todo: sb-cga's normalize introduces a lot of floating point error,
;;; so this rounds it to something decent. Replacing it with a slower
;;; but more accurate calculation is *definitely* more ideal.
(defun triangle-normal (vec1 vec2 vec3)
  (flet ((float-round (number)
           (/ (round number (/ 1 1000)) 1000.0)))
    (map 'vector
         #'float-round
         (normalize (cross-product (vec- vec2 vec1) (vec- vec3 vec1))))))

;;; Averages the triangle-normals
(defun compute-vertex-normals (triangle-normals)
  (mapcar (lambda (x)
            (/ x (length triangle-normals)))
          (reduce (lambda (x y)
                    (map 'list #'+ x y))
                  triangle-normals
                  :initial-value #(0.0 0.0 0.0))))

(defun attribute3 (state attribute a b c)
  (with-accessors ((attributes attributes))
      state
    (replace (getf attributes attribute) (vector a b c))))

(defun attribute2 (state attribute a b)
  (with-accessors ((attributes attributes))
      state
    (replace (getf attributes attribute) (vector a b))))

(defun vertex3 (state x y z)
  (with-accessors ((array vertex-array)
                   (counter vertex-counter)
                   (attributes attributes)
                   (stride stride))
      state
    (setf (aref array (+ 0 counter)) x
          (aref array (+ 1 counter)) y
          (aref array (+ 2 counter)) z)
    (let ((i (+ 3 counter)))
      (doplist (attribute-name attribute-array attributes)
        (dotimes (j (length attribute-array))
          (setf (aref array i) (elt attribute-array j))
          (incf i)))
      (incf counter stride))))

(defun begin (state &key vertices elements stride type attributes)
  (with-accessors ((vertex-count vertex-count)
                   (element-count element-count)
                   (stored-stride stride)
                   (vertex-array vertex-array)
                   (element-array element-array)
                   (attribute-plist attributes))
      state
    (setf vertex-count vertices
          element-count elements
          stored-stride stride
          vertex-array (make-array (* stride vertices) :element-type type)
          element-array (make-array elements :element-type 'fixnum)
          attribute-plist (make-list (length attributes)))
    (do ((i 0 (+ 2 i)))
        ((= i (length attributes)))
      (setf (elt attribute-plist i) (elt attributes i))
      (setf (elt attribute-plist (1+ i)) (make-array (elt attributes (1+ i)) :element-type type)))))

;; fixme: this is probably not ideal
(defun begin-elements (state &rest elements)
  (with-accessors ((element-array element-array)
                   (element-place-counter element-place-counter)
                   (element-number-counter element-number-counter))
      state
    (map nil
         (lambda (element)
           ;; needed to silence an sbcl warning on (elt elements i)
           (check-type element fixnum))
         elements)
    (dotimes (i (length elements))
      (setf (aref element-array (+ element-place-counter i)) (+ element-number-counter (elt elements i))))
    (incf element-place-counter (length elements))
    (setf element-number-counter (+ element-number-counter
                                    1
                                    (reduce (lambda (e1 e2)
                                              (if (> (+ element-number-counter e1)
                                                     (+ element-number-counter e2))
                                                  e1
                                                  e2))
                                            elements
                                            :initial-value 0)))))

;;; todo: this will have meaning when error checking becomes a thing
(defun end-elements (state)
  (declare (ignore state))
  "stub")

;;; todo: make sure there were enough vertex3 calls
(defun end (state)
  state)

(defun quad-face (state x1 y1 z1 x2 y2 z2 x3 y3 z3 x4 y4 z4 s1 t1 s2 t2 s3 t3 s4 t4 direction)
  (let ((normal (case direction
                  (:y- (vec 0f0 -1f0 0f0))
                  (:y+ (vec 0f0 1f0 0f0))
                  (:x- (vec -1f0 0f0 0f0))
                  (:x+ (vec 1f0 0f0 0f0))
                  (:z- (vec 0f0 0f0 -1f0))
                  (:z+ (vec 0f0 0f0 1f0))
                  (t direction))))
    (begin-elements state 0 1 2 2 3 0)
    (attribute3 state :normal (elt normal 0) (elt normal 1) (elt normal 2))
    (attribute2 state :texcoord s1 t1)
    (vertex3 state x1 y1 z1)
    (attribute2 state :texcoord s2 t2)
    (vertex3 state x2 y2 z2)
    (attribute2 state :texcoord s3 t3)
    (vertex3 state x3 y3 z3)
    (attribute2 state :texcoord s4 t4)
    (vertex3 state x4 y4 z4)
    (end-elements state)))

(defun square-2d (color)
  (let ((x1 -0.5f0)
         (x2 0.5f0)
         (y1 -0.5f0)
        (y2 0.5f0))
    (let ((state (make-instance 'model)))
      (begin state :vertices 4 :elements 6 :stride +stride+ :type 'single-float :attributes '(:color 3 :texcoord 2))
      (begin-elements state 0 1 2 2 3 0)
      (attribute3 state :color (aref color 0) (aref color 1) (aref color 2))
      (attribute2 state :texcoord 0f0 0f0)
      (vertex3 state x1 y1 0f0)
      (attribute2 state :texcoord 1f0 0f0)
      (vertex3 state x2 y1 0f0)
      (attribute2 state :texcoord 1f0 1f0)
      (vertex3 state x2 y2 0f0)
      (attribute2 state :texcoord 0f0 1f0)
      (vertex3 state x1 y2 0f0)
      (end-elements state)
      (end state))))

(defun hexagon-2d (color)
  (let* ((r (/ (sqrt 3f0) 2f0))
         (x1 (- 0.5f0))
         (x2 (- 0.25f0))
         (x3 (- x2))
         (x4 (- x1))
         (y1 (* 0.5f0 r))
         (y2 0f0)
         (y3 (- y1)))
    (let ((state (make-instance 'model)))
      (begin state :vertices 6 :elements 12 :stride +stride+ :type 'single-float :attributes '(:color 3 :texcoord 2))
      (begin-elements state 0 1 2 2 3 4 4 5 0 0 2 4)
      (attribute3 state :color (aref color 0) (aref color 1) (aref color 2))
      (attribute2 state :texcoord (+ x1 0.5f0) (+ (/ y2 r) 0.5f0))
      (vertex3 state x1 y2 0f0)
      (attribute2 state :texcoord (+ x2 0.5f0) (+ (/ y3 r) 0.5f0))
      (vertex3 state x2 y3 0f0)
      (attribute2 state :texcoord (+ x3 0.5f0) (+ (/ y3 r) 0.5f0))
      (vertex3 state x3 y3 0f0)
      (attribute2 state :texcoord (+ x4 0.5f0) (+ (/ y2 r) 0.5f0))
      (vertex3 state x4 y2 0f0)
      (attribute2 state :texcoord (+ x3 0.5f0) (+ (/ y1 r) 0.5f0))
      (vertex3 state x3 y1 0f0)
      (attribute2 state :texcoord (+ x2 0.5f0) (+ (/ y1 r) 0.5f0))
      (vertex3 state x2 y1 0f0)
      (end-elements state)
      (end state))))

(defun rectangle-xy (state x1 x2 y1 y2 z1 z2 direction)
  (let ((length (abs (- x2 x1)))
        (width (abs (- z2 z1))))
    (quad-face state
               x2 y2 z1
               x1 y1 z1
               x1 y1 z2
               x2 y2 z2
               length 0f0
               0f0 0f0
               0f0 width
               length width
               direction)))

(defun rectangle-yz (state x1 x2 y1 y2 z1 z2 direction)
  (let ((length (abs (- x2 x1)))
        (width (abs (- z2 z1))))
    (quad-face state
               x2 y2 z1
               x1 y2 z1
               x1 y1 z2
               x2 y1 z2
               length 0f0
               0f0 0f0
               0f0 width
               length width
               direction)))

(defun rectangle-xz (state x1 x2 y1 y2 z1 z2 direction)
  (let ((length (abs (+ (- x2 x1) (- z2 z1))))
        (width (abs (- y2 y1))))
    (quad-face state
               x1 y1 z2
               x1 y2 z2
               x2 y2 z1
               x2 y1 z1
               0f0 0f0
               0f0 width
               length width
               length 0f0
               direction)))

(defun make-cuboid (x-min x-max y-min y-max z-min z-max)
  (let ((state (make-instance 'model))
        (faces 6))
    (begin state :vertices (* 4 faces) :elements (* 6 faces) :stride +stride+ :type 'single-float :attributes '(:normal 3 :texcoord 2))
    (rectangle-xy state x-min x-max y-max y-max z-min z-max :y+)
    (rectangle-xy state x-max x-min y-min y-min z-min z-max :y-)
    (rectangle-xz state x-min x-max y-min y-max z-min z-min :z-)
    (rectangle-xz state x-max x-min y-min y-max z-max z-max :z+)
    (rectangle-xz state x-min x-min y-min y-max z-min z-max :x-)
    (rectangle-xz state x-max x-max y-min y-max z-max z-min :x+)
    (end state)))

(defun make-cube (size)
  (let ((half-size (* size 0.5)))
    (make-cuboid (- half-size) half-size
                 (- half-size) half-size
                 (- half-size) half-size)))

;;; Walls, floors, and ceilings are (for now) defined as 3D shapes
;;; with rectangle sides. Most of the sides will not be turned into
;;; geometry, only those visible in the level. The visible sides are
;;; given by direction. The direction is the side it's *facing*.
;;;
;;; todo: also generate quads rather than rectangles for e.g. door
;;; frames connecting two rooms with different floor heights
(defun wall-rectangle-with-direction (state x-min x-max y-min y-max z-min z-max direction)
  (ecase direction
    (:y- (rectangle-xy state x-min x-max y-min y-min z-max z-min direction))
    (:y+ (rectangle-xy state x-min x-max y-max y-max z-min z-max direction))
    (:x- (rectangle-xz state x-min x-min y-min y-max z-min z-max direction))
    (:x+ (rectangle-xz state x-max x-max y-min y-max z-max z-min direction))
    (:z- (rectangle-xz state x-min x-max y-min y-max z-min z-min direction))
    (:z+ (rectangle-xz state x-max x-min y-min y-max z-max z-max direction))))

(defun wall-rectangles-with-direction (state x-min x-max y-min y-max z-min z-max direction-list)
  ;; (unless *remove-hidden-wall-surfaces*
  ;;   (setf direction-list (list :y- :y+ :x- :x+ :z- :z+)))
  (dolist (direction direction-list)
    (wall-rectangle-with-direction state x-min x-max y-min y-max z-min z-max direction)))

;;; just a simple ramp to test mixing quads and triangles
;;;
;;; todo: remove the floor/wall that this blocks
(defun ramp (state x-min x-max y-min y-max z-min z-max)
  (begin-elements state 0 1 2)
  (attribute3 state :normal 0f0 0f0 1f0)
  (attribute2 state :texcoord 0f0 0f0)
  (vertex3 state x-max y-min z-min)
  (attribute2 state :texcoord 1f0 0f0)
  (vertex3 state x-min y-min z-min)
  (attribute2 state :texcoord 1f0 1f0)
  (vertex3 state x-min y-max z-min)
  (end-elements state)
  (quad-face state
             x-max y-min z-max
             x-max y-min z-min
             x-min y-max z-min
             x-min y-max z-max
             0f0 0f0
             1f0 0f0
             1f0 1f0
             0f0 1f0
             ;; fixme: actually calculate the normal
             (vec 0.25f0 0.75f0 0f0)))

(defun make-platforming-level ()
  (let ((state (make-instance 'model))
        (quad-faces 42)
        (tri-faces 0))
    (begin state :vertices (+ (* 4 quad-faces) (* 3 tri-faces)) :elements (+ (* 6 quad-faces) (* 3 tri-faces)) :stride +stride+ :type 'single-float :attributes '(:normal 3 :texcoord 2))
    (wall-rectangles-with-direction state -5.0 5.0 -10.0 0.0 -5.0 5.0 (list :y+ :x- :x+ :z- :z+))
    (wall-rectangle-with-direction state 5.0 16.0 -10.0 -10.0 -7.0 2.0 :y+)
    (wall-rectangle-with-direction state 5.0 6.0 -10.0 -10.0 2.0 5.0 :y+)
    (wall-rectangles-with-direction state 6.0 9.0 -10.0 0.0 2.0 5.0 (list :y+ :x- :x+ :z- :z+))
    (wall-rectangle-with-direction state 9.0 10.0 -10.0 -10.0 2.0 5.0 :y+)
    (wall-rectangles-with-direction state 10.0 13.0 -10.0 0.0 2.0 5.0 (list :y+ :x- :x+ :z- :z+))
    (wall-rectangle-with-direction state 13.0 14.0 -10.0 -10.0 2.0 5.0 :y+)
    (wall-rectangles-with-direction state 14.0 17.0 -10.0 0.0 2.0 5.0 (list :y+ :x- :x+ :z- :z+))
    (wall-rectangle-with-direction state 16.0 17.0 -10.0 -10.0 -2.0 2.0 :y+)
    (wall-rectangles-with-direction state 16.0 17.0 -10.0 0.0 -2.0 1.0 (list :y+ :x- :x+ :z- :z+))
    (wall-rectangle-with-direction state 16.0 17.0 -10.0 -10.0 -3.5 -2.0 :y+)
    (wall-rectangles-with-direction state 16.0 17.0 -10.0 0.0 -5.5 -3.5 (list :y+ :x- :x+ :z- :z+))
    (wall-rectangle-with-direction state 16.0 17.0 -10.0 -10.0 -7.0 -5.5 :y+)
    (wall-rectangles-with-direction state 13.0 17.0 -10.0 0.0 -9.0 -7.0 (list :y+ :x- :x+ :z- :z+))
    (end state)))

;;; Defines level geometry based on walls, floors, and ceilings. For
;;; the sake of simplification, they're all considered as
;;; "walls". Only the sides that are visible within the level need to
;;; be rendered. e.g. the y+ side is visible from being above in the
;;; y-axis.
(defun make-level (wall-thickness floor-thickness room-x-width ceiling-height room-z-width door-offset door-width door-height)
  (let* ((door-0 (+ door-offset (- (* 0.5 door-width))))
         (door-1 (+ door-offset (* 0.5 door-width)))
         (room-0-x-width room-x-width)
         (room-1-x-width room-x-width)
         (room-2-x-width room-x-width)
         (x0 -5f0)
         (x0-0 (- x0 wall-thickness))
         (x0-1 (+ door-0 (/ room-0-x-width 2f0) x0))
         (x0-2 (+ door-1 (/ room-0-x-width 2f0) x0))
         (x1 (+ room-0-x-width x0))
         (x1-0 (+ x1 wall-thickness))
         (x1-1 (+ door-0 (/ room-1-x-width 2f0) x1-0))
         (x1-2 (+ door-1 (/ room-1-x-width 2f0) x1-0))
         (x2 (+ room-1-x-width x1-0))
         (x2-0 (+ x2 wall-thickness))
         (x2-1 (+ door-0 (/ room-2-x-width 2f0) x2-0))
         (x2-2 (+ door-1 (/ room-2-x-width 2f0) x2-0))
         (x3 (+ room-2-x-width x2-0))
         (x3-0 (+ x3 wall-thickness))
         (y0 0f0)
         (y0-0 (- y0 floor-thickness))
         (y0-1 (+ y0 door-height))
         (y1 (+ y0 ceiling-height))
         (y1-1 (+ y1 floor-thickness))
         (y2 (+ ceiling-height y1-1))
         (y2-0 (+ y2 floor-thickness))
         (z-1 (- (* 2 (- room-z-width)) 5f0 (* 2 wall-thickness)))
         (z-1-0 (- z-1 wall-thickness))
         (z0 (- -5f0 wall-thickness))
         (z0-0 (- z0 wall-thickness))
         (z1 0f0)
         (z1-0 (- z1 wall-thickness))
         (z1-1 (+ door-0 (/ room-z-width 2f0) z1))
         (z1-2 (+ door-1 (/ room-z-width 2f0) z1))
         (z2 (+ room-z-width z1))
         (z2-0 (+ z2 wall-thickness)))
    (let ((state (make-instance 'model))
          (quad-faces 109)
          (tri-faces 0))
      (begin state :vertices (+ (* 4 quad-faces) (* 3 tri-faces)) :elements (+ (* 6 quad-faces) (* 3 tri-faces)) :stride +stride+ :type 'single-float :attributes '(:normal 3 :texcoord 2))
      (wall-rectangles-with-direction state x0 x3 y0-0 y0 z0 z1-0 (list :y+))
      (wall-rectangles-with-direction state x0 x3 y1 y1-1 z0 z1-0 (list :y-))
      (wall-rectangles-with-direction state x0 x1-1 y0 y1 z0-0 z0 (list :z+ :z-))
      (wall-rectangles-with-direction state x1-2 x3 y0 y1 z0-0 z0 (list :z+ :z-))
      (wall-rectangles-with-direction state x1-1 x1-2 y0-1 y1 z0-0 z0 (list :z+ :z- :y-))
      (wall-rectangles-with-direction state x1-1 x1-2 y0-0 y0 z0-0 z0 (list :y+))
      (wall-rectangle-with-direction state x1-1 x1-1 y0 y0-1 z0-0 z0 :x+)
      (wall-rectangle-with-direction state x1-2 x1-2 y0 y0-1 z0-0 z0 :x-)
      (wall-rectangles-with-direction state x0 x3 y1 y2-0 z0-0 z0 (list :z-))
      (wall-rectangles-with-direction state x0-0 x0 y0 y1 z0 z1-0 (list :x+))
      (wall-rectangles-with-direction state x3 x3-0 y0 y1 z0 z1-0 (list :x-))
      (wall-rectangles-with-direction state x0 x0-1 y0 y1 z1-0 z1 (list :z+ :z-))
      (wall-rectangles-with-direction state x0-1 x0-2 y0-1 y1 z1-0 z1 (list :z+ :z- :y-))
      (wall-rectangles-with-direction state x0-2 x1 y0 y1 z1-0 z1 (list :z- :z+))
      (wall-rectangles-with-direction state x0-1 x0-2 y0-0 y0 z1-0 z1 (list :y+))
      (wall-rectangle-with-direction state x0-1 x0-1 y0 y0-1 z1-0 z1 :x+)
      (wall-rectangle-with-direction state x0-2 x0-2 y0 y0-1 z1-0 z1 :x-)
      (wall-rectangles-with-direction state x0 x1 y0-0 y0 z1 z2 (list :y+))
      (wall-rectangles-with-direction state x0 x1 y1 y1-1 z1 z2 (list :y- :y+))
      (wall-rectangles-with-direction state x0-0 x0 y0 y1 z1 z2 (list :x+))
      (wall-rectangles-with-direction state x1 x1-0 y0 y1 z1 z1-1 (list :x- :x+))
      (wall-rectangles-with-direction state x1 x1-0 y0-1 y1 z1-1 z1-2 (list :x- :x+ :y-))
      (wall-rectangles-with-direction state x1 x1-0 y0-1 y0 z1-1 z1-2 (list :y+))
      (wall-rectangle-with-direction state x1 x1-0 y0 y0-1 z1-1 z1-1 :z+)
      (wall-rectangle-with-direction state x1 x1-0 y0 y0-1 z1-2 z1-2 :z-)
      (wall-rectangles-with-direction state x1 x1-0 y0 y1 z1-2 z2 (list :x- :x+))
      (wall-rectangles-with-direction state x1 x1-0 y0 y1 z1-0 z1 (list :z-))
      (wall-rectangles-with-direction state x0 x1 y0 y1 z2 z2-0 (list :z-))
      (wall-rectangles-with-direction state x0 x1 y2 y2-0 z1 z2 (list :y-))
      (wall-rectangles-with-direction state x0 x1 y1-1 y2 z1-0 z1 (list :z+))
      (wall-rectangles-with-direction state x0 x1 y1-1 y2 z2 z2-0 (list :z-))
      (wall-rectangles-with-direction state x0-0 x0 y1-1 y2 z1 z2 (list :x+))
      (wall-rectangles-with-direction state x1 x1-0 y1-1 y2 z2 z2-0 (list :z-))
      (wall-rectangles-with-direction state x1 x1-0 y1-1 y2 z1-0 z1 (list :z+))
      (wall-rectangles-with-direction state x1 x1-0 y2 y2-0 z1 z2 (list :y-))
      (wall-rectangles-with-direction state x1 x1-0 y1 y1-1 z1 (+ z1 2f0) (list :y+))
      (wall-rectangles-with-direction state x1 x1-0 y1 y1-1 (+ z1 2f0) z2 (list :x+ :y+))
      (wall-rectangles-with-direction state x1 x1-0 y1 y1-1 (- z2 2f0) z2 (list :y+))
      (wall-rectangles-with-direction state x1-0 x2 y0-0 y0 z1 z2 (list :y+))
      (wall-rectangles-with-direction state x1-0 x2 y2 y2-0 z1 z2 (list :y-))
      (wall-rectangles-with-direction state x1-0 x2 y1 y1-1 z1 (+ z1 2f0) (list :z+ :y- :y+))
      ;; (ramp state x1-0 (* 0.5f0 (+ x2 x1-0)) y0 y1-1 (- z2 2f0) z2)
      (wall-rectangles-with-direction state x1-0 x1-1 y0 y1 z1-0 z1 (list :z+ :z-))
      (wall-rectangles-with-direction state x1-1 x1-2 y0-1 y1 z1-0 z1 (list :z+ :z- :y-))
      (wall-rectangles-with-direction state x1-2 x2 y0 y1 z1-0 z1 (list :z- :z+))
      (wall-rectangles-with-direction state x1-1 x1-2 y0-0 y0 z1-0 z1 (list :y+))
      (wall-rectangle-with-direction state x1-1 x1-1 y0 y0-1 z1-0 z1 :x+)
      (wall-rectangle-with-direction state x1-2 x1-2 y0 y0-1 z1-0 z1 :x-)
      (wall-rectangles-with-direction state x1-0 x2 y1-1 y2 z1-0 z1 (list :z+))
      (wall-rectangles-with-direction state x1-0 x2 y0 y2 z2 z2-0 (list :z-))
      (wall-rectangles-with-direction state x2 x2-0 y0 y1 z1 z1-1 (list :x- :x+))
      (wall-rectangles-with-direction state x2 x2-0 y0-1 y1 z1-1 z1-2 (list :x- :x+ :y-))
      (wall-rectangles-with-direction state x2 x2-0 y0-1 y0 z1-1 z1-2 (list :y+))
      (wall-rectangle-with-direction state x2 x2-0 y0 y0-1 z1-1 z1-1 :z+)
      (wall-rectangle-with-direction state x2 x2-0 y0 y0-1 z1-2 z1-2 :z-)
      (wall-rectangles-with-direction state x2 x2-0 y0 y1 z1-2 z2 (list :x- :x+))
      (wall-rectangles-with-direction state x2 x2-0 y0 y1 z1-0 z1 (list :z-))
      (wall-rectangles-with-direction state x2 x2-0 y1 y1-1 z1 (+ z1 2f0) (list :y+))
      (wall-rectangles-with-direction state x2 x2-0 y1 y1-1 (+ z1 2f0) z2 (list :x- :y+))
      (wall-rectangles-with-direction state x2-0 x3 y0-0 y0 z1 z2 (list :y+))
      (wall-rectangles-with-direction state x2-0 x3 y1 y1-1 z1 z2 (list :y- :y+))
      (wall-rectangles-with-direction state x2 x2-0 y1-1 y2 z2 z2-0 (list :z-))
      (wall-rectangles-with-direction state x2 x2-0 y1-1 y2 z1-0 z1 (list :z+))
      (wall-rectangles-with-direction state x2 x2-0 y2 y2-0 z1 z2 (list :y-))
      (wall-rectangles-with-direction state x2-0 x3 y2 y2-0 z1 z2 (list :y-))
      (wall-rectangles-with-direction state x2-0 x3 y1-1 y2 z1-0 z1 (list :z+))
      (wall-rectangles-with-direction state x2-0 x3 y1-1 y2 z2 z2-0 (list :z-))
      (wall-rectangles-with-direction state x3 x3-0 y1-1 y2 z1 z2 (list :x-))
      (wall-rectangles-with-direction state x2-0 x2-1 y0 y1 z1-0 z1 (list :z+ :z-))
      (wall-rectangles-with-direction state x2-1 x2-2 y0-1 y1 z1-0 z1 (list :z+ :z- :y-))
      (wall-rectangles-with-direction state x2-2 x3 y0 y1 z1-0 z1 (list :z- :z+))
      (wall-rectangles-with-direction state x2-1 x2-2 y0-0 y0 z1-0 z1 (list :y+))
      (wall-rectangle-with-direction state x2-1 x2-1 y0 y0-1 z1-0 z1 :x+)
      (wall-rectangle-with-direction state x2-2 x2-2 y0 y0-1 z1-0 z1 :x-)
      (wall-rectangles-with-direction state x2-0 x3 y0 y1 z2 z2-0 (list :z-))
      (wall-rectangles-with-direction state x3 x3-0 y0 y1 z1 z2 (list :x-))
      (wall-rectangles-with-direction state x0 x3 y0-0 y0 z-1 z0-0 (list :y+))
      (wall-rectangles-with-direction state x0 x3 y0 y1 z-1-0 z-1 (list :z+))
      (wall-rectangles-with-direction state x0-0 x0 y0 y1 z-1 z0-0 (list :x+))
      (wall-rectangles-with-direction state x3 x3-0 y0 y1 z-1 z0-0 (list :x-))
      (end state))))

(defun make-hexagon ()
  (hexagon-2d (make-array 3 :element-type 'single-float :initial-contents #(0.85f0 0.25f0 0f0))))

(defun make-square ()
  (square-2d (make-array 3 :element-type 'single-float :initial-contents #(0.9f0 0f0 0f0))))

;;;; Textures

;;;; fixme: temporary texture generation, temporarily put here

;;; fixme: temporary, no need for a class once gl-data is rewritten
(defclass texture ()
  ((texture-data :initarg :texture-data
                 :accessor texture-data)))

;;; Generates a simple texture that draws a square around the whole
;;; image, offset by a border
(defun generate-texture ()
  (let* ((texel-size 4) ; rgba
         ;; lower this if you want to use the debug print statements
         (texture-size 10)
         (row-size (expt 2 texture-size))
         (actual-row-size (* texel-size row-size))
         (image-size (* row-size row-size))
         (texture (make-array (* texel-size image-size) :element-type '(unsigned-byte 8) :initial-element 255))
         (line-thickness 6)
         (border-thickness 1))
    ;; iterate over each row
    (do ((i 0 (+ i actual-row-size)))
        ((= i (* image-size texel-size)) texture)
      (let ((column-position (/ i row-size)))
        ;; iterate over each texel
        (do ((j 0 (+ j texel-size)))
            ((= j actual-row-size))
          (let ((row-position (mod (+ i j) (* texel-size row-size))))
            ;; (format t "(~D, ~D): " column-position row-position)
            ;; the first part of the and sets the border, the second sets the thickness
            (if (and (and (< (* texel-size (- border-thickness 1)) column-position (* texel-size (- row-size border-thickness)))
                          (< (* texel-size (- border-thickness 1)) row-position (* texel-size (- row-size border-thickness))))
                     (or (<= (* texel-size 1) row-position (* texel-size line-thickness))
                         (<= (* texel-size 1) column-position (* texel-size line-thickness))
                         (<= (* texel-size (- row-size line-thickness 1)) row-position (* texel-size (- row-size 1 1)))
                         (<= (* texel-size (- row-size line-thickness 1)) column-position (* texel-size (- row-size 1 1)))))
                (let ((foo (+ 230 (random 26))))
                  (setf (aref texture (+ i j 0)) foo
                        (aref texture (+ i j 1)) foo
                        (aref texture (+ i j 2)) foo))
                (let ((foo (+ 4 (random 26)))) ; 16
                  (setf (aref texture (+ i j 0)) foo
                        (aref texture (+ i j 1)) foo
                        (aref texture (+ i j 2)) foo))))
          ;; (format t "#(~D ~D ~D ~D) " (aref texture (+ i j 0)) (aref texture (+ i j 1)) (aref texture (+ i j 2)) (aref texture (+ i j 3)))
          ))
      ;; (format t "~%")
      )))

(defun generate-textures ()
  (list (cons :square (make-instance 'texture :texture-data (generate-texture)))))

(defpackage #:zombie-raptor/data/game-data
  (:use #:cl
        #:zombie-raptor/data/shader)
  (:import-from #:alexandria #:plist-hash-table)
  (:import-from #:uiop/filesystem #:directory-files)
  (:import-from #:uiop/pathname #:merge-pathnames*)
  (:import-from #:zombie-raptor/data/data-path #:main-data-directory)
  (:export #:extension-table
           #:read-sxp
           #:write-sxp
           #:import-sxp-data
           #:find-key-bindings
           #:load-data))

(in-package #:zombie-raptor/data/game-data)

;;;; Defines .SXP file format

(defun read-sxp (data-path &key (read-eval nil))
  "Reads an s-expression (sxp) file at data-path using the Common Lisp
reader. Every s-expression in the file is placed into one
list. Everything read is literal (as if quoted), with no evaluation
unless read-eval is enabled."
  (let ((*read-eval* read-eval))
    (with-open-file (file-stream data-path :direction :input)
      (with-input-from-string (start "(")
        (with-input-from-string (end ")")
          (read (make-concatenated-stream start file-stream end)))))))

(defun write-sxp (data-path sxp &key (if-exists :error) (read-case :invert))
  "Writes an s-expression (sxp) file at the ensured path
data-path. sxp must be a list, and it should be structured so that
`read-sxp' can read it. if-exists supports a keyword from the long
list of if-exists keywords that `open' uses.

read-case is a readtable case keyword, which is :invert by default so
that the capital letters are turned into the more readable lower case
forms. They will then be converted to upper case again when `read-sxp'
is run. If you need this operation to work exactly like `read-sxp',
use :upcase instead, but it will be less readable by humans."
  (let ((*readtable* (copy-readtable nil)))
    (setf (readtable-case *readtable*) read-case)
    (with-open-file (file-stream (ensure-directories-exist data-path)
                                 :direction :output
                                 :if-exists if-exists)
      (map nil
           (lambda (expression)
             (write expression :stream file-stream)
             (write-char #\Newline file-stream))
           sxp))
    sxp))

(defun import-sxp-data (path)
  "Imports structured data from an sxp file. The first line must
be (:s-expression name) where name is the type of s-expression, to
dispatch on. Optionally, there can be more arguments past name. If
there are, they are passed in as additional arguments and must be
handled there."
  (let ((data (read-sxp path)))
    (cons (pathname-name path)
          (when (and (listp (car data)) (eql (caar data) :s-expression))
            ;; todo: optional options in a plist at (cddar data)
            (case (cadar data)
              ;; todo: store layout name, store other layouts, switch between layouts
              (:key-bindings (when (eql (caadr data) :key-bindings)
                               (cddadr data))))))))

;;;; Loads data

(defun import-shader-data (path pathname-type type)
  (cons (concatenate 'string (pathname-name path) "-" pathname-type)
        (make-shader type path)))

(defun find-key-bindings (game-data)
  (cdr (find-if (lambda (data)
                  (string= (car data) "key-bindings"))
                game-data)))

(defun load-data (data-path)
  "Loads data files from data directories in an ASDF system into an
internal form."
  (delete nil
          (mapcar (lambda (path)
                    (let ((pathname-type (pathname-type path)))
                      (cond ((string-equal pathname-type "frag")
                             (import-shader-data path pathname-type :fragment-shader))
                            ((string-equal pathname-type "vert")
                             (import-shader-data path pathname-type :vertex-shader))
                            ((string-equal pathname-type "sxp")
                             (import-sxp-data path)))))
                  (directory-files (apply #'main-data-directory data-path)))))

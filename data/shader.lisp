(defpackage #:zombie-raptor/data/shader
  (:use #:cl)
  (:import-from #:alexandria #:read-file-into-string)
  (:export #:make-shader
           #:shader
           #:shader-source
           #:shader-type))

(in-package #:zombie-raptor/data/shader)

(defclass shader ()
  ((source
    :initarg :source
    :accessor shader-source
    :initform (error "You need to provide the source for the shader."))
   (shader-type
    :initarg :type
    :accessor shader-type
    :initform (error "You need to provide the type for the shader."))))

(defun make-shader (shader-type path)
  (make-instance 'shader
                 :type shader-type
                 :source (read-file-into-string path)))

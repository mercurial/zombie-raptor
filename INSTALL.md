Installation
============

Instructions on how to make Quicklisp recognize projects not in
Quicklisp (including this one!) are available
[here](https://www.quicklisp.org/beta/faq.html#local-project).

CL Dependencies
---------------

### utility libraries
- alexandria
- named-readtables
- rutils
- uiop

### vector math libraries
- sb-cga

### libraries for talking to C
- cffi
- cl-opengl
- cl-sdl2
- cl-sdl2-mixer *(not currently in Quicklisp!)*

C Dependencies
--------------

- OpenGL 3.3 or later
- SDL2
- SDL2_mixer

CL Implementations
------------------

If it doesn't work on a supported platform, it's a bug. Tests will be
run on all supported CL implementations when the game engine is more
mature.

The main requirements of an implementation are:
- ASDF 3.1.2 or later
- CFFI
- Threads
- Unicode (for chat and localization)

| Implementation | Supported | CFFI | Notes                      |
| -------------- |:---------:|:----:| -------------------------- |
| SBCL           | Yes       | Yes  | Recommended (for now)      |
| CCL            | Yes       | Yes  |                            |
| ECL            | Yes       | Yes  |                            |
| ABCL           | No        | Yes  |                            |
| Clasp          | No        | No   | [GH Issue #162](https://github.com/drmeister/clasp/issues/162) |
| CLISP          | No        | Yes  | Outdated ASDF by default   |
| CMUCL          | No        | Yes  | No Unicode?                |
| GCL            | No        | No?  |                            |
| Mezzano        | No        | No   | No 3D acceleration         |
| MKCL           | No        | Yes  |                            |
| Allegro        | No        | Yes  | Commercial                 |
| Genera         | No        | No   | Commercial                 |
| LispWorks      | No        | Yes  | Commercial                 |
| mocl           | No        | No?  | Commercial                 |
| Scieneer       | No        | No?  | Commercial                 |

Contributing to Zombie Raptor
=============================

Style
-----

- In general, this project follows the
  [Google Common Lisp Style Guide](https://google.github.io/styleguide/lispguide.xml).

- **Documentation**. Everything that is exported should have a
  docstring. Everything else should be commented if it is
  non-obvious. Functions that are large enough to need comments might
  be better off as multiple smaller functions that are obvious.

- **Indentation**. As the style guide says, use the indentation that a
  properly configured emacs + SLIME uses.
  - The only exception is where reader-macros like #h() break the
    auto-indent. If that happens, the second line will have to be
    manually indented to line up properly with the line above, but
    then the rest should be properly auto-indented after that.
  - SXP files also tend to break the auto-indent because of the
    implicit quoting. When indenting them, treat the s-expressions as
    if they're quoted.

- **Lambdas**. lambdas are written without the #'

- **Line length**. As in the Google style guide, 100 characters is the
  line length limit.

- **List vs. arrays**. This is a game engine. Take memory layout into
  account instead of just the O(n) random access. Where possible, use
  arrays that have types that optimized CL implementations can
  upgrade.
  - Generally, this means to use flat arrays that hold bit, byte, or
    other numerical types where possible. The main exception is
    integer because it is CL's bignum and so it doesn't get upgraded.
  - Use bits instead of booleans in these arrays because booleans
    cannot be upgraded in SBCL.
  - You can see if your CL implementation supports the upgrade by
    running (upgraded-array-element-type 'foo), where 'foo is what you
    would put in :element-type in make-array. If it returns T, nothing
    is happening when you're specifying the element type.
  - This isn't as important in areas that do not need to be as fast,
    but it is very important if it's run every iteration of the game
    loop.

- **CLOS**. Where performance is crucial, use composition over
  inheritance. CLOS should only be used in the very high level stuff
  or in stuff that is rarely run.
  - Perhaps some MOP magic can reduce the overhead of CLOS so it can
    be used in more places. Make sure to benchmark it if you try it.

- **Functional?** The degree to which the programming paradigm is
  mostly functional generally depends on how close the code is to a C
  library. The GL and SDL code is messy and full of setting and
  state. The rest of the code is written in a mostly functional style,
  avoiding setf, mutation, etc., where possible. The main place
  outside of the input and render code where setting might be
  unavoidable is when dealing with upgraded vectors.

- **Packages**. This library uses the package-inferred-system from
  ASDF 3. Each file is its own package, with its own imports and
  exports at the top of the file rather than in a separate
  package.lisp file.
  - See
    [this blog post](http://davazp.net/2014/11/26/modern-library-with-asdf-and-package-inferred-system.html)
    and
    [this section of the ASDF documentation](https://common-lisp.net/project/asdf/asdf/The-package_002dinferred_002dsystem-extension.html).

- **Tests**. For now, the input and render code do not have to have
  automated tests. (This is why there are no tests at the moment. This
  area is the focus of the engine right now.)

- **Side effects**. In areas of the code that are mostly functional in
  style, functions with side effects use the Scheme convention of
  ending the function name with an exclamation mark. For instance,
  foo! instead of foo.

Libraries
---------

- **Licenses**. Zombie Raptor is permissively licensed. CL libraries
  and all of their dependencies must be under a permissive
  (non-copyleft) license, generally either an MIT or BSD license. Even
  weak copyleft must be avoided. Licenses should be FSF or OSI
  approved rather than niche licenses. Other licenses can work for
  certain C libraries, but not for their CL wrappers.

- **Wrapped C or C++ Libraries?** CL libraries that depend on C or C++
  libraries should be avoided if possible. The only exceptions so far
  are:
  - Creating a window (SDL2)
  - Detecting input (SDL2)
  - 3D graphics (OpenGL)
  - Audio (SDL2_mixer?)
  - Fonts (Pango?)
  - 3D Mesh Importing (might be needed in the future)
  - Networking (might be needed in the future)
  - Interfacing with Steam (will probably be needed in the future)

- **NIH?** Not invented here (NIH) is acceptable in the following
  circumstances:
  - The license of the library doesn't pass the guidelines above.
  - The NIH code must be considerably faster and/or considerably less
    complicated than the existing alternative.
  - The library is very incomplete, but only if it is easier to write
    similar functionality from scratch rather than extend the
    incomplete library.
  - The library is very opinionated and doesn't integrate well with
    this game engine or is incompatible with other libraries that the
    engine uses.
  - The library depends on a C or C++ library and an alternative can
    be written in CL that performs acceptably. (Just because it's
    possible doesn't mean that it should be prioritized!)
  - The library is for 2D stuff. This is a 3D game engine.
  - The library is abandoned.

Graphics
--------

- **OpenGL 3.3 vs 4.5**. OpenGL 4.5 will be used once libmesa supports
  it. See [this](https://people.freedesktop.org/~imirkin/glxinfo/) for
  more information. The switch to OpenGL 4.x will probably happen in
  Zombie Raptor 18 because it will significantly increase the system
  requirements for running Zombie Raptor games.

- **GLSL vs SPIR-V**. SPIR-V will be the preferred way to do shaders
  once OpenGL supports SPIR-V via ARB_gl_spirv. That way, OpenGL and
  the future Vulkan renderer can use the same shaders. The use of GLSL
  shaders is temporary, depending on ZR adopting one of several
  attempts at transpiler and/or SPIR-V compiler languages. ZR will
  *not* introduce its own shader language (if possible) because that
  would violate the NIH rule.

- **Vulkan**. It's more important to get a working game engine first
  and cl-vulkan isn't ready yet. Adding a Vulkan renderer is a planned
  long term goal, possibly as soon as Zombie Raptor 19.

Version numbers
---------------

The version scheme used by Zombie Raptor is year.major.minor.patch,
where major.minor.patch follow [semver](http://semver.org/) and the
year version is periodically updated when necessary.

Once the engine is stable, the year-version will start at 17 for the
year 2017 CE. It will always take 2000 less than the number of the
year when it is released. The year-version will never increment more
than once in a year, but it might not increment every year. When a new
year-version becomes stable, it resets the major version to 1.

The extra version number is added over semver because the version
requirements of game engines are different from the version
requirements of most software libraries. Sometimes *very* major
updates can happen that would never happen to most kinds of stable
libraries because games aren't expected to necessarily be updated to
new versions of the engine. Like major versions, year-versions will
always be backwards incompatible in some way. Things like major
overhauls of the render code (e.g. switching to Vulkan) could
increment the year-version.

At the moment, the version is listed as 0.0.0.0. This is because the
game engine is too unstable to even have a version number right
now. Once releases start happening, the version will become 0.0.1.0
and climb from there. The release candidates for Zombie Raptor 17 will
be 17.0.minor.patch and the first stable version of Zombie Raptor 17
will be numbered 17.1.

Any libraries spun off of Zombie Raptor will use semver without this
additional year qualification and start with the version number of 1.

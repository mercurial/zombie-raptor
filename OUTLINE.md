Outline
=======

Outline of current directories
------------------------------

### audio
- **sdl2-mixer.lisp** - handles music and sound

### core
- **controls.lisp** - functions that are called via key and mouse actions
- **example.lisp** - a tiny application that uses the engine
- **input.lisp** - handles input that SDL detects
- **sdl2.lisp** - detects input via SDL and handles SDL windows
- **window.lisp** - the game window and game loop

### data
- **data-path.lisp** - handles portable pathnames and directories
- **game-data.lisp** - defines the SXP file format and loads data
    files by extension
- **model.lisp** - defines the model SXP format
- **shader.lisp** - reads shaders into an object

### entity
- **entity.lisp** - the core of the entity component system (ECS)
- **physics.lisp** - the physics system; uses the **physics** directory
- **render.lisp** - the rendering system; uses the **render** directory

### math
- **math.lisp** - miscellaneous useful math functions required by the engine
- **matrix.lisp** - various matrices that are needed
- **quaternion.lisp** - an implementation of quaternions built on sb-cga

### physics
- **physics.lisp** - 3D game physics

### render
- **gl-data.lisp** - the gl-data object
- **gl.lisp** - 3D rendering in OpenGL

### **test-data**
Some simple data files to read

Outline of planned directories
------------------------------

- **ai** - a basic game AI system
- **chat** - handles in-game text chat (for multiplayer)
- **logging** - logging support
- **multiplayer** - networking and multiplayer support
- **scripting** - interfaces with sandboxed scripting languages
  - **scheme.lisp** - interfaces with a sandboxed Scheme
- **shader** - a shader compiler, when SPIR-V support is added
- **text** - handles text and font rendering
- **translation** - handles text translation
- **ui** - a WIMP GUI and a HUD

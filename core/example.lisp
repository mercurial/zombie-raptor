(defpackage #:zombie-raptor/core/example
  (:use #:cl)
  (:import-from #:sb-cga #:vec)
  (:import-from #:zombie-raptor/core/window
                #:make-window)
  (:import-from #:zombie-raptor/data/model
                #:make-hexagon
                #:make-square
                #:make-cube
                #:make-cuboid
                #:make-level
                #:make-platforming-level)
  (:import-from #:zombie-raptor/entity/entity
                #:make-basic-entity
                #:make-fps-camera-entity)
  (:export #:launch-example-window
           #:launch-platforming-test))

(in-package #:zombie-raptor/core/example)

;;;; Generates and populates the main test game

(defun generate-models ()
  (list (cons :hexagon-2d (make-hexagon))
        (cons :square-2d (make-square))
        (cons :cube (make-cube 1))
        (cons :tall-box (make-cuboid -0.5 0.5 -1.0 1.0 -0.5 0.5))
        (cons :elevator-platform (make-cuboid -1.5 1.5 -0.125 0.125 -1.5 1.5))
        (cons :world (make-level 0.125 0.125 10.0 2.75 10.0 0.0 2.4 2.1))))

(defun make-test-entities (&key ecs)
  (make-fps-camera-entity ecs :location (vec 0f0 1.75f0 5f0))
  (make-basic-entity ecs :cube
                     :location (vec -4f0 0.5f0 -2.625f0)
                     :acceleration (vec 0.1f0 0.0f0 0f0))
  (make-basic-entity ecs :cube :location (vec -4f0 0.5f0 1f0))
  (make-basic-entity ecs :cube :location (vec 4.5f0 0.5f0 8f0))
  (make-basic-entity ecs :elevator-platform :location (vec 13.625f0 0.125f0 8.5f0))
  (make-basic-entity ecs :elevator-platform :location (vec 6.625f0 0.125f0 8.5f0))
  (make-basic-entity ecs :tall-box :location (vec -3f0 1.0f0 3.5f0))
  (make-basic-entity ecs :tall-box :location (vec -3f0 1.0f0 -9f0))
  (make-basic-entity ecs :tall-box :location (vec 6f0 1.0f0 -11f0))
  (make-basic-entity ecs :cube :location (vec 9f0 0.5f0 -8f0))
  (make-basic-entity ecs :world)
  nil)

(defun make-test-hud (&key ecs width height)
  (make-basic-entity ecs :square-2d :scale (vec 2f0 2f0 1f0))
  (make-basic-entity ecs :hexagon-2d :location (vec (- (/ width 2.5f0)) (- (/ height 2.75f0)) 0f0) :scale (vec (/ height 10f0) (/ height 10f0) 1f0))
  nil)

(defun launch-example-window (&key (width 1280) (height 720) fullscreen debug)
  "Launches a window that runs a simple game built on the Zombie
Raptor engine."
  (make-window :width width
               :height height
               :fullscreen fullscreen
               :title "Zombie Raptor Example Program"
               :data-path '("zombie-raptor" "test-data")
               :shader-programs '(:default ("simple-vert" "simple-frag")
                                  :hud ("simple-2d-vert" "simple-2d-frag"))
               :models (generate-models)
               :ecs-init-function #'make-test-entities
               :hud-ecs-init-function #'make-test-hud
               :msaa 4
               :debug debug))

;;;; Generates and populates a test platforming game

(defun generate-platforming-test-models ()
  (list (cons :square-2d (make-square))
        (cons :platform (make-cuboid -1.5 1.5 -0.125 0.125 -1.5 1.5))
        (cons :world (make-platforming-level))))

(defun make-platforming-test-entities (&key ecs)
  (make-fps-camera-entity ecs :location (vec 0f0 1.75f0 0f0) :rotation (vec -1f0 0f0 0f0))
  (make-basic-entity ecs :platform :location (vec 11f0 -0.125f0 -8.5f0))
  (make-basic-entity ecs :world))

(defun make-simple-test-hud (&key ecs width height)
  (declare (ignore width height))
  (make-basic-entity ecs :square-2d :scale (vec 2f0 2f0 1f0))
  nil)

(defun launch-platforming-test (&key (width 1280) (height 720) fullscreen debug)
  "A simple platforming game."
  (make-window :width width
               :height height
               :fullscreen fullscreen
               :title "Zombie Raptor Platforming Test"
               :data-path '("zombie-raptor" "test-data")
               :shader-programs '(:default ("simple-vert" "simple-frag")
                                  :hud ("simple-2d-vert" "simple-2d-frag"))
               :models (generate-platforming-test-models)
               :ecs-init-function #'make-platforming-test-entities
               :hud-ecs-init-function #'make-simple-test-hud
               :msaa 4
               :debug debug))

(defpackage #:zombie-raptor/core/controls
  (:use #:cl)
  (:import-from #:named-readtables
                #:in-readtable)
  (:import-from #:rutils.readtable
                #:rutils-readtable)
  (:import-from #:sb-cga #:vec)
  (:import-from #:zombie-raptor/core/sdl2
                #:push-quit)
  (:import-from #:zombie-raptor/entity/entity
                #:entity-jump
                #:move-entity
                #:rotate-entity-yaw
                #:rotate-entity-pitch
                #:rotate-entity-roll
                #:rotate-entity-camera-pitch)
  (:import-from #:zombie-raptor/math/math
                #:+single-pi+)
  (:export #:key-actions
           #:mouse-actions))

(in-package #:zombie-raptor/core/controls)

;;; Gives the literal #h hash table syntax
(in-readtable rutils-readtable)

(defparameter *sensitivity-adjustment* 0.002f0)

(defun key-actions (ecs toggle-mouse-rotation)
  (let ((entity-id 0)
        (move-quantity 0.04f0)
        (rotation-quantity (* 0.0025f0 +single-pi+)))
    #h(:quit #'push-quit
       :use-entity-0 (lambda (&key &allow-other-keys)
                       (setf entity-id 0)
                       nil)
       :use-entity-1 (lambda (&key &allow-other-keys)
                       (setf entity-id 1)
                       nil)
       :use-entity-2 (lambda (&key &allow-other-keys)
                       (setf entity-id 2)
                       nil)
       :use-entity-3 (lambda (&key &allow-other-keys)
                       (setf entity-id 3)
                       nil)
       :use-entity-4 (lambda (&key &allow-other-keys)
                       (setf entity-id 4)
                       nil)
       :use-entity-5 (lambda (&key &allow-other-keys)
                       (setf entity-id 5)
                       nil)
       :move-strafe-left (lambda (&key &allow-other-keys)
                           (move-entity ecs entity-id (vec (- move-quantity) 0f0 0f0))
                           t)
       :move-strafe-right (lambda (&key &allow-other-keys)
                            (move-entity ecs entity-id (vec move-quantity 0f0 0f0))
                            t)
       :move-backwards (lambda (&key &allow-other-keys)
                         (move-entity ecs entity-id (vec 0f0 0f0 move-quantity))
                         t)
       :move-forwards (lambda (&key &allow-other-keys)
                        (move-entity ecs entity-id (vec 0f0 0f0 (- move-quantity)))
                        t)
       :jump (lambda (&key &allow-other-keys)
               (entity-jump ecs entity-id 3.6f0)
               t)
       :move-y- (lambda (&key &allow-other-keys)
                  (move-entity ecs entity-id (vec 0f0 (- move-quantity) 0f0))
                  t)
       :move-y+ (lambda (&key &allow-other-keys)
                  (move-entity ecs entity-id (vec 0f0 move-quantity 0f0))
                  t)
       :rotate- (lambda (&key &allow-other-keys)
                  (rotate-entity-yaw ecs entity-id (- rotation-quantity))
                  t)
       :rotate+ (lambda (&key &allow-other-keys)
                  (rotate-entity-yaw ecs entity-id rotation-quantity)
                  t)
       :pitch-rotate- (lambda (&key &allow-other-keys)
                        (rotate-entity-pitch ecs entity-id (- rotation-quantity))
                        t)
       :pitch-rotate+ (lambda (&key &allow-other-keys)
                        (rotate-entity-pitch ecs entity-id rotation-quantity)
                        t)
       :roll-rotate- (lambda (&key &allow-other-keys)
                       (rotate-entity-roll ecs entity-id (- rotation-quantity))
                       t)
       :roll-rotate+ (lambda (&key &allow-other-keys)
                       (rotate-entity-roll ecs entity-id rotation-quantity)
                       t)
       :toggle-mouse (lambda (&key &allow-other-keys)
                       (funcall toggle-mouse-rotation)
                       nil))))

(defun mouse-actions (ecs invert-controls mouse-sensitivity width height)
  (let ((entity-id 0)
        (rotation-quantity (if invert-controls
                               (* *sensitivity-adjustment* mouse-sensitivity)
                               (* *sensitivity-adjustment* (- mouse-sensitivity)))))
    (list (lambda (x-distance)
            (rotate-entity-yaw ecs entity-id (* x-distance rotation-quantity)))
          (lambda (y-distance)
            (rotate-entity-camera-pitch ecs entity-id (* y-distance (/ height width) rotation-quantity))))))

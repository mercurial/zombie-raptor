;;;; Game Input

(defpackage #:zombie-raptor/core/input
  (:use #:cl)
  (:import-from #:alexandria
                #:doplist)
  (:import-from #:sdl2
                #:get-event-type
                #:next-event
                #:scancode-key-to-value
                #:scancode-value
                #:set-relative-mouse-mode
                #:warp-mouse-in-window)
  (:import-from #:sdl2-ffi
                #:sdl-event)
  (:import-from #:plus-c
                #:c-let)
  (:export #:input
           #:make-input-state
           #:reset-input-state!
           #:setup-key-bindings
           #:sdl-input!
           #:toggle-mouse-rotation-function))

(in-package #:zombie-raptor/core/input)

;;;; Input state constants and types

(defconstant +sdl-scancode-size+ 283
  "The size of arrays whose indexes represent SDL scancodes.")

(defconstant +sdl-mouse-buttons+ 5
  "These are the mouse buttons that SDL supports.")

(deftype keyboard-state-array ()
  `(simple-bit-vector ,+sdl-scancode-size+))

(deftype mouse-click-state-array ()
  `(simple-bit-vector ,(1+ +sdl-mouse-buttons+)))

(deftype mouse-click-counter-array ()
  `(simple-array fixnum (,(1+ +sdl-mouse-buttons+))))

(deftype click-location-array ()
  '(simple-array fixnum (4)))

(deftype mouse-motion-array ()
  '(simple-array fixnum (3)))

(deftype mouse-wheel-array ()
  '(simple-array fixnum (2)))

(deftype key-bindings-array ()
  `(simple-vector ,+sdl-scancode-size+))

;;; This struct should only be modified in the sdl-input! and
;;; reset-input-state! functions. Other functions should only read its
;;; contents.
(defstruct input-state
  (key-presses   (make-array +sdl-scancode-size+
                             :element-type 'bit
                             :initial-element 0)
                 :type keyboard-state-array)
  (keys-active   (make-array +sdl-scancode-size+
                             :element-type 'bit
                             :initial-element 0)
                 :type keyboard-state-array)
  (mouse-clicks  (make-array (1+ +sdl-mouse-buttons+)
                             :element-type 'bit
                             :initial-element 0)
                 :type mouse-click-state-array)
  (click-counter (make-array (1+ +sdl-mouse-buttons+)
                             :element-type 'fixnum
                             :initial-element 0)
                 :type mouse-click-counter-array)
  (click-location (make-array 4
                              :element-type 'fixnum
                              :initial-element 0)
                  :type click-location-array)
  (mouse-motion   (make-array 3
                              :element-type 'fixnum
                              :initial-element 0)
                  :type mouse-motion-array)
  (mouse-wheel   (make-array 2
                             :element-type 'fixnum
                             :initial-element 0)
                 :type mouse-wheel-array)
  (mouse-rotation t :type boolean)
  (warp-mouse nil :type boolean))

(defun sdl-input! (input-state events)
  "Handles SDL events, which are mostly input-related. This function
modifies input-state and returns t if the quit event was received."
  ;; (declare (optimize (speed 3) (safety 0) (debug 0)))
  (declare (input-state input-state))
  (do ((eventp (next-event events :poll) (next-event events :poll))
       quit)
      ((zerop eventp) quit)
    (c-let ((event sdl-event :from events))
      (case (get-event-type events)
        (:keydown
         (let ((keysym (event :key :keysym)))
           (setf (bit (input-state-key-presses input-state) (scancode-value keysym)) 1)
           (setf (bit (input-state-keys-active input-state) (scancode-value keysym)) 1)))
        (:keyup
         (let ((keysym (event :key :keysym)))
           (setf (bit (input-state-key-presses input-state) (scancode-value keysym)) 0)
           (setf (bit (input-state-keys-active input-state) (scancode-value keysym)) 0)))
        (:mousebuttondown
         (let ((button (event :button :button))
               (clicks (event :button :clicks))
               (x (event :button :x))
               (y (event :button :y)))
           (setf (bit (input-state-mouse-clicks input-state) button) 1
                 (aref (input-state-click-counter input-state) button) clicks
                 (aref (input-state-click-location input-state) 0) x
                 (aref (input-state-click-location input-state) 1) y)))
        (:mousebuttonup
         (let ((button (event :button :button))
               (x (event :button :x))
               (y (event :button :y)))
           (setf (bit (input-state-mouse-clicks input-state) button) 0
                 (aref (input-state-click-location input-state) 2) x
                 (aref (input-state-click-location input-state) 3) y)))
        (:mousemotion
         (let ((x-rel (event :motion :xrel))
               (y-rel (event :motion :yrel))
               (state (event :motion :state)))
           (incf (aref (input-state-mouse-motion input-state) 0) x-rel)
           (incf (aref (input-state-mouse-motion input-state) 1) y-rel)
           ;; todo: this probably needs to be handled differently, if
           ;; it needs to be handled at all
           (setf (aref (input-state-mouse-motion input-state) 2) state)))
        (:mousewheel
         (let ((x (event :wheel :x))
               (y (event :wheel :y))
               (direction (event :wheel :direction)))
           (declare (ignore direction))
           (incf (aref (input-state-mouse-wheel input-state) 0) x)
           (incf (aref (input-state-mouse-wheel input-state) 1) y)))
        (:quit
         (setf quit t))))))

(defun setup-key-bindings (key-bindings-list key-actions)
  "Takes a plist indexed by scancode keywords and converts it into an
array indexed by scancode integer values."
  ;; (declare (optimize (speed 3) (safety 0) (debug 0)))
  (declare (list key-bindings-list) (hash-table key-actions))
  (let ((controls (make-array +sdl-scancode-size+ :initial-element nil)))
    (doplist (key val key-bindings-list controls)
      (setf (aref controls (scancode-key-to-value key)) (gethash val key-actions)))))

(defun input (input-state key-bindings mouse-actions)
  "Handles the input (key and mouse) state."
  ;; (declare (optimize (speed 3) (safety 0) (debug 0)))
  (declare (input-state input-state) (key-bindings-array key-bindings))
  (let ((key-presses (input-state-key-presses input-state))
        (keys-active (input-state-keys-active input-state)))
    (dotimes (i +sdl-scancode-size+)
      (when (and (= (bit key-presses i) 1)
                 (= (bit keys-active i) 1)
                 (aref key-bindings i))
        (let ((keep-active-p (funcall (the function (aref key-bindings i)))))
          (unless keep-active-p
            (setf (bit keys-active i) 0)))))
    (when (input-state-mouse-rotation input-state)
      (let ((x-rel (aref (input-state-mouse-motion input-state) 0))
            (y-rel (aref (input-state-mouse-motion input-state) 1)))
        (when (not (zerop x-rel))
          (funcall (elt mouse-actions 0) x-rel))
        (when (not (zerop y-rel))
          (funcall (elt mouse-actions 1) y-rel))))))

(defun reset-input-state! (input-state sdl-window width height)
  ;; (declare (optimize (speed 3) (safety 0) (debug 0)))
  (declare (input-state input-state))
  (when (input-state-warp-mouse input-state)
    (warp-mouse-in-window sdl-window (round (/ width 2)) (round (/ height 2)))
    (setf (input-state-warp-mouse input-state) nil))
  (fill (input-state-mouse-motion input-state) 0)
  (fill (input-state-mouse-wheel input-state) 0))

(defun toggle-mouse-rotation-function (input-state)
  (lambda ()
    (let ((mouse-rotate-p (input-state-mouse-rotation input-state)))
      (setf (input-state-mouse-rotation input-state) (not mouse-rotate-p))
      (if mouse-rotate-p
          (progn (set-relative-mouse-mode 0)
                 (setf (input-state-warp-mouse input-state) t))
          (set-relative-mouse-mode 1)))))

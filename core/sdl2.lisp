;;;; SDL2 Window and Input

(defpackage #:zombie-raptor/core/sdl2
  (:use #:cl #:sdl2)
  (:import-from #:cl-opengl
                #:clear
                #:clear-color
                #:flush)
  (:export #:exit-sdl
           #:get-window-aspect-ratio
           #:get-window-size
           #:in-main-thread
           #:push-quit
           #:set-relative-mouse-mode
           #:setup-sdl
           #:warp-mouse-in-window
           #:with-sdl-event
           #:with-sdl-rendering))

(in-package #:zombie-raptor/core/sdl2)

;;; OpenGL 3.3
(defconstant +gl-major-version+ 3)
(defconstant +gl-minor-version+ 3)

(defun setup-sdl (title width height vsync msaa &optional fullscreen)
  "Starts SDL and then creates and returns an sdl window and an SDL GL
context."
  (init :everything)
  (in-main-thread (:no-event t)
    (gl-set-attr :context-profile-mask 1)
    (gl-set-attr :context-major-version +gl-major-version+)
    (gl-set-attr :context-minor-version +gl-minor-version+)
    (gl-set-attr :multisamplebuffers (if (zerop msaa) 0 1))
    (gl-set-attr :multisamplesamples msaa)
    ;; (gl-set-attr :stencil-size 8) ; todo
    (let* ((sdl-window (create-window :title title
                                      :x :centered
                                      :y :centered
                                      :w width
                                      :h height
                                      :flags '(:shown :opengl)))
           (gl-context (gl-create-context sdl-window)))
      (gl-make-current sdl-window gl-context)
      (gl-set-swap-interval (if vsync 1 0))
      (set-window-fullscreen sdl-window (when fullscreen :fullscreen-desktop))
      (set-relative-mouse-mode 1)
      (values sdl-window gl-context))))

(defun exit-sdl (gl-context sdl-window)
  "Deletes the SDL GL context, destroys the SDL window, and then quits
SDL."
  (gl-delete-context gl-context)
  (destroy-window sdl-window)
  (quit))

(defmacro with-sdl-rendering ((sdl-window) &body body)
  "Runs the basic SDL+GL rendering code on sdl-window."
  `(progn
     (clear-color 0f0 0f0 0f0 1f0)
     (clear :color-buffer :depth-buffer)
     ,@body
     (flush)
     (gl-swap-window ,sdl-window)))

(defun push-quit (&key &allow-other-keys)
  "Pushes the :quit SDL event when called."
  (push-event :quit)
  t)

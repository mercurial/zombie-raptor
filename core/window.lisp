;;;; Game Window

(defpackage #:zombie-raptor/core/window
  (:use #:cl
        #:zombie-raptor/core/sdl2
        #:zombie-raptor/core/input)
  (:import-from #:alexandria
                #:define-constant)
  (:import-from #:zombie-raptor/core/controls
                #:key-actions
                #:mouse-actions)
  (:import-from #:zombie-raptor/data/model
                #:generate-textures)
  (:import-from #:zombie-raptor/data/game-data
                #:find-key-bindings
                #:load-data)
  (:import-from #:zombie-raptor/entity/entity
                #:make-entity-component-system
                #:store-old-states)
  (:import-from #:zombie-raptor/entity/physics
                #:physics)
  (:import-from #:zombie-raptor/entity/render
                #:render-2d-hud
                #:render-entities)
  (:import-from #:zombie-raptor/math/matrix
                #:make-ortho
                #:make-projection-matrix)
  (:import-from #:zombie-raptor/render/gl-data
                #:cleanup-gl-data
                #:shader-program
                #:meshes
                #:make-shader-programs
                #:make-gl-data)
  (:export #:+year-version+
           #:+major-version+
           #:+minor-version+
           #:+patch-version+
           #:+version-string+
           #:fps
           #:make-window
           #:version
           #:window))

(in-package #:zombie-raptor/core/window)

;;;; Version Numbers

(defconstant +year-version+ 0)
(defconstant +major-version+ 0)
(defconstant +minor-version+ 0)
(defconstant +patch-version+ 0)
(define-constant +version-string+ "0.0.0.0" :test 'string=)

;;;; Time

(defconstant +time-step+ 0.01d0
  "The time step (in seconds) is used by the real time physics
  simulation's discrete approximations of physics.")

(defconstant +float-time-per-second+ (coerce internal-time-units-per-second 'double-float)
  "This is used by the get-second function to convert between
  implementation-specific internal time and double-float seconds.")

(defun get-second ()
  "Converts internal CL time to a double-float second."
  (/ (get-internal-real-time) +float-time-per-second+))

;;;; Game window

;;;; This window stores important state and runs the game loop in the
;;;; background. For things that need to be single threaded,
;;;; `in-main-thread' must be used.

(defclass window ()
  ((fps
    :accessor fps
    :initform 0f0
    :documentation "An estimation of the current frames per second")
   (sdl-window
    :accessor sdl-window
    :documentation "An SDL window foreign object")
   (gl-context
    :accessor gl-context
    :documentation "An SDL GL context")
   (gl-data
    :accessor gl-data
    :documentation "GL data that needs to persist")
   (ecs
    :accessor ecs
    :documentation "An entity component system")
   (hud-ecs
    :accessor hud-ecs
    :documentation "Another entity component system")
   (version
    :initform +version-string+
    :reader version))
  (:documentation "A game window that stores all the state that needs
  to be stored while also running a game loop in a background
  process. Create it with make-window."))

(defun make-window (&key
                      (title "Zombie Raptor")
                      (width 1280)
                      (height 720)
                      fullscreen
                      data-path
                      models
                      shader-programs
                      ecs-init-function
                      hud-ecs-init-function
                      (mouse-sensitivity 1)
                      (invert-controls nil)
                      (fovy 45f0)
                      (vsync t)
                      (msaa 0)
                      debug)
  (let ((window (make-instance 'window))
        ;; fixme: do this temporarily until gl-data is rewritten
        (data   (nconc models (generate-textures) (load-data data-path))))
    (with-accessors ((ecs ecs)
                     (hud-ecs hud-ecs)
                     (sdl-window sdl-window)
                     (gl-context gl-context)
                     (gl-data gl-data))
        window
      (setf ecs (make-entity-component-system)
            hud-ecs (make-entity-component-system)
            (values sdl-window gl-context) (setup-sdl title width height vsync msaa fullscreen))
      (in-main-thread (:no-event t)
        (setf gl-data (make-gl-data data :start-gl t :debug debug))
        (make-shader-programs gl-data shader-programs :debug debug))
      (in-main-thread (:background t)
        (multiple-value-bind (width height)
            (get-window-size sdl-window)
          (let ((input-state (make-input-state)))
            (unwind-protect
                 (progn (funcall ecs-init-function :ecs ecs)
                        (funcall hud-ecs-init-function :ecs hud-ecs :width width :height height)
                        (game-loop ecs
                                   hud-ecs
                                   input-state
                                   sdl-window
                                   gl-data
                                   (make-projection-matrix fovy (get-window-aspect-ratio sdl-window))
                                   (make-ortho width height)
                                   (setup-key-bindings (find-key-bindings data) (key-actions ecs (toggle-mouse-rotation-function input-state)))
                                   (mouse-actions ecs invert-controls mouse-sensitivity width height)
                                   window
                                   width
                                   height))
              (exit-sdl gl-context sdl-window)
              (cleanup-gl-data gl-data)
              (setf (fps window) 0d0))))))
    window))

;;; temporary function
(defun location-events (ecs game-events)
  (let ((location (zombie-raptor/entity/entity::locations-location (zombie-raptor/entity/entity::entity-component ecs 1 :location))))
    (when (and (elt game-events 0) (> (elt location 0) 5f0))
      (let ((acceleration (zombie-raptor/entity/entity::physics-acceleration (zombie-raptor/entity/entity::entity-component ecs 1 :physics))))
        (setf (elt game-events 0) nil)
        (setf (elt acceleration 0) (- (elt acceleration 0)))))
    (when (and (not (elt game-events 0)) (< (elt location 0) 0f0))
      (let ((acceleration (zombie-raptor/entity/entity::physics-acceleration (zombie-raptor/entity/entity:entity-component ecs 1 :physics))))
        (setf (elt game-events 0) t)
        (setf (elt acceleration 0) (- (elt acceleration 0)))))))

;;; fixme: some of the arguments passed in are temporary
(defun game-loop (ecs hud-ecs input-state sdl-window gl-data projection-matrix hud-matrix key-bindings mouse-actions window width height)
  (with-sdl-event (events)
    (do ((quit (sdl-input! input-state events))
         (shader-program (shader-program gl-data :default))
         (hud-shader-program (shader-program gl-data :hud))
         (meshes (meshes gl-data))
         (camera-id 0)
         (time (get-second))
         (time-diff 0.0d0)
         (elapsed-time 0.0d0)
         (missed-time 0.0d0)
         (game-events (list t)))
        (quit)
      (let ((new-time (get-second))
            (old-time-diff time-diff))
        (setf time-diff (- new-time time)
              time new-time
              (fps window) (/ (* 0.5d0 (+ time-diff old-time-diff))))
        (incf missed-time time-diff))
      (do ()
          ((< missed-time +time-step+))
        (store-old-states ecs)
        (setf quit (sdl-input! input-state events))
        (input input-state key-bindings mouse-actions)
        (reset-input-state! input-state sdl-window width height)
        (physics ecs (coerce elapsed-time 'single-float) (coerce +time-step+ 'single-float))
        (location-events ecs game-events)
        (incf elapsed-time +time-step+)
        (decf missed-time +time-step+))
      (with-sdl-rendering (sdl-window)
        (render-2d-hud hud-ecs meshes hud-shader-program hud-matrix (coerce (/ missed-time +time-step+) 'single-float))
        (render-entities ecs meshes shader-program projection-matrix camera-id (coerce (/ missed-time +time-step+) 'single-float))))))

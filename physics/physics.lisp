(defpackage #:zombie-raptor/physics/physics
  (:use #:cl)
  (:import-from #:sb-cga
                #:vec)
  (:export #:acceleration
           #:update-location-and-velocity!))

(in-package #:zombie-raptor/physics/physics)

(defun two-value-rk4 (time y z h f)
  "An implementation of the RK4 algorithm."
  (declare (optimize (speed 3) (safety 0) (debug 0)))
  (declare (single-float time) (single-float y) (single-float z) (single-float h) (function f))
  (let ((h/2 (* 0.5f0 h)))
    (flet ((midpoint (y k)
             (declare (single-float y) (single-float k))
             (+ y (* h/2 k)))
           (end (y k)
             (declare (single-float y) (single-float k))
             (+ y (* h k)))
           (result (y k1 k2 k3 k4)
             (declare (single-float y) (single-float k1) (single-float k2)
                      (single-float k3) (single-float k4))
             (+ y (* h (/ 6.0f0) (+ k1 (* 2f0 (+ k2 k3)) k4)))))
      (multiple-value-bind (k1-y k1-z)
          (funcall f time y z)
        (multiple-value-bind (k2-y k2-z)
            (funcall f (+ time h/2) (midpoint y k1-y) (midpoint z k1-z))
          (multiple-value-bind (k3-y k3-z)
              (funcall f (+ time h/2) (midpoint y k2-y) (midpoint z k2-z))
            (multiple-value-bind (k4-y k4-z)
                (funcall f (+ time h) (end y k3-y) (end z k3-z))
              (values (result y k1-y k2-y k3-y k4-y)
                      (result z k1-z k2-z k3-z k4-z)))))))))

;;; Modifies each element of the location and velocity vectors by
;;; applying the RK4 algorithm on each part of those vectors.
(defun update-location-and-velocity! (location velocity acceleration time time-step acceleration-function)
  (declare (optimize (speed 3) (safety 0) (debug 0)))
  (declare (single-float time) (single-float time-step) (vec location) (vec velocity)
           (vec acceleration) (function acceleration-function))
  (dotimes (i 3)
    (setf (values (aref location i) (aref velocity i))
          (two-value-rk4 time (aref location i) (aref velocity i) time-step
                         (the function (funcall acceleration-function (aref acceleration i)))))))

;;; A temporary acceleration function to test out the physics system.
(defun acceleration (a)
  (declare (optimize (speed 3) (safety 0) (debug 0)))
  (declare (single-float a))
  (lambda (time x v)
    (declare (single-float time) (single-float x) (single-float v))
    (declare (ignore time) (ignore x))
    (values v a)))

;;;; OpenGL Data

(defpackage #:zombie-raptor/render/gl-data
  (:use #:cl
        #:zombie-raptor/data/model
        #:zombie-raptor/data/shader
        #:zombie-raptor/render/gl)
  (:import-from #:cl-opengl)
  (:import-from #:alexandria
                #:doplist
                #:maphash-values)
  (:export #:gl-data
           #:make-gl-data
           #:cleanup-gl-data
           #:meshes
           #:make-shader-programs
           #:shader-program
           #:buffers
           #:setup-gl))

(in-package #:zombie-raptor/render/gl-data)

;;;; This file is a very high-level abstraction over render/gl,
;;;; designed to be used with core/window for starting OpenGL and
;;;; holding OpenGL data structures that need to persist throughout
;;;; the life of the program.

(defclass gl-data ()
  ((meshes
    :accessor meshes
    :initform (make-hash-table))
   (textures
    :accessor textures
    :initform (make-hash-table))
   ;; fixme: temporary
   (texture-count
    :accessor texture-count
    :initform 1)
   (compiled-shaders
    :accessor compiled-shaders
    :initform (make-hash-table :test 'equal))
   (buffers
    :initarg :buffers
    :accessor buffers)
   (allocated-buffers
    :accessor allocated-buffers
    :initform 0)
   (programs
    :accessor programs
    :initform (make-hash-table))))

(defun find-compiled-shader (compiled-shaders shader-name)
  (gethash shader-name compiled-shaders))

(defun find-compiled-shaders (compiled-shaders shader-names)
  (mapcar (lambda (shader-name)
            (find-compiled-shader compiled-shaders shader-name))
          shader-names))

(defun make-shader-program (programs compiled-shaders program-name shader-names &optional debug)
  (setf (gethash program-name programs)
        (link-gl-program (find-compiled-shaders compiled-shaders shader-names) debug)))

(defun make-shader-programs (gl-data shader-programs &key debug)
  (with-accessors ((programs programs)
                   (compiled-shaders compiled-shaders))
      gl-data
    (doplist (program-key shader-names shader-programs)
             (make-shader-program programs compiled-shaders program-key shader-names debug))))

(defun shader-program (gl-data name)
  (with-accessors ((programs programs))
      gl-data
    (gethash name programs)))

(defun texture (gl-data name)
  (with-accessors ((textures textures))
      gl-data
    (gethash name textures)))

(defun load-into-gl-data (gl-data &optional debug)
  (with-accessors ((meshes meshes)
                   (textures textures)
                   (texture-count texture-count)
                   (compiled-shaders compiled-shaders)
                   (buffers buffers)
                   (buffer-count allocated-buffers))
      gl-data
    ;; todo: rewrite
    (lambda (data)
      (typecase (cdr data)
        (model (setf (gethash (car data) meshes)
                     (make-vao (cdr data)
                               (subseq buffers buffer-count (+ buffer-count 2))))
               (incf buffer-count 2))
        (texture (setf (gethash (car data) textures)
                       (progn
                         (let ((texture-number texture-count))
                           (make-gl-texture texture-count (texture-data (cdr data)))
                           (incf texture-count)
                           texture-number))))
        (shader (setf (gethash (car data) compiled-shaders)
                      (compile-shader-object (cdr data) debug)))))))

(defun generate-buffers-for-models (data)
  (gl:gen-buffers (* 2 (count-if (lambda (x)
                                   (typep (cdr x) 'model))
                                 data))))

(defun make-gl-data (loaded-data &key start-gl debug)
  (when start-gl (setup-gl))
  (let ((gl-data (make-instance 'gl-data :buffers (generate-buffers-for-models loaded-data))))
    (gl:gen-textures (count-if (lambda (x)
                                 (typep (cdr x) 'texture))
                               loaded-data))
    (map nil (load-into-gl-data gl-data debug) loaded-data)
    gl-data))

(defun cleanup-gl-data (gl-data)
  (with-accessors ((buffers buffers)
                   (programs programs)
                   (compiled-shaders compiled-shaders))
      gl-data
    (maphash-values #'gl:delete-shader compiled-shaders)
    (maphash-values #'gl:delete-program programs)
    (gl:delete-buffers buffers)
    ;; gl:delete-vertex-arrays
    (setf compiled-shaders nil
          programs nil
          buffers nil)))

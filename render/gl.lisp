;;;; OpenGL

(defpackage #:zombie-raptor/render/gl
  (:use #:cl
        #:zombie-raptor/data/model
        #:zombie-raptor/data/shader)
  (:import-from #:cl-opengl)
  (:import-from #:cffi)
  (:export #:glsl-name
           #:setup-gl
           #:with-shader-program
           #:vao
           #:make-vao
           #:draw
           #:shader
           #:compile-shader-object
           #:compile-gl-shader
           #:link-gl-program
           #:uniform-matrix
           #:uniform-vector
           #:make-gl-texture))

(in-package #:zombie-raptor/render/gl)

(defgeneric make-vao (object buffers))

(defconstant +float-size+ (cffi:foreign-type-size :float))
(defparameter *max-texture-max-anisotropy-ext* 1f0)

;;;; Convenience macros and functions

(defun glsl-name (symbol)
  "Converts a Lisp symbol to a GLSL identifier."
  (let ((camelcase-string (cffi:translate-camelcase-name symbol :special-words '("ID" "1D" "2D" "3D" "MS"))))
    (if (and (> (length camelcase-string) 2)
             (string= camelcase-string "gl" :end1 2)
             (upper-case-p (elt camelcase-string 2)))
        (concatenate 'string "gl_" (subseq camelcase-string 2))
        camelcase-string)))

(defun setup-gl ()
  "Sets up GL. An SDL window and a GL context need to be created
before you run this, and this must be run in the main SDL thread."
  (gl:enable :depth-test :cull-face) ; todo: :stencil-test
  (setf *max-texture-max-anisotropy-ext* (gl:get-float :max-texture-max-anisotropy-ext 1))
  (gl:hint :line-smooth-hint :nicest)
  (gl:hint :polygon-smooth-hint :nicest))

(defmacro with-shader-program ((program) &body body)
  `(unwind-protect
        (progn (gl:use-program ,program)
               ,@body)
     (gl:use-program 0)))

;;;; OpenGL VAO

(defclass vao ()
  ((array-buffer
    :initarg :array-buffer
    :accessor array-buffer)
   (element-array-buffer
    :initarg :element-array-buffer
    :accessor element-array-buffer)
   (size
    :initarg :size
    :accessor size)))

;;; todo: Check to see if there's a better approach
(defun lisp-array-to-gl-array (type lisp-array)
  (let* ((length (length lisp-array))
         (gl-array (gl:alloc-gl-array type length)))
    (dotimes (i length gl-array)
      (setf (gl:glaref gl-array i) (elt lisp-array i)))))

(defun bind-gl-buffers (array-buffer element-array-buffer vertex-data elements)
  (gl:bind-vertex-array (gl:gen-vertex-array))
  (gl:bind-buffer :array-buffer array-buffer)
  (gl:buffer-data :array-buffer
                  :static-draw
                  (lisp-array-to-gl-array :float vertex-data))
  (gl:bind-buffer :element-array-buffer element-array-buffer)
  (gl:buffer-data :element-array-buffer :static-draw (lisp-array-to-gl-array :unsigned-short elements)))

(defmethod make-vao ((model model) buffers)
  (let ((vertex-array (vertex-array model))
        (element-array (element-array model))
        (array-buffer (elt buffers 0))
        (element-array-buffer (elt buffers 1)))
    (bind-gl-buffers array-buffer element-array-buffer vertex-array element-array)
    (make-instance 'vao
                   :array-buffer array-buffer
                   :element-array-buffer element-array-buffer
                   :size (length element-array))))

(defun draw (vao texture-id program)
  (gl:active-texture :texture0)
  (gl:bind-texture :texture-2d texture-id)
  ;; fixme: unhardcode
  (gl:uniformi (gl:get-uniform-location program "loadedTexture") 0)
  (gl:bind-buffer :array-buffer (array-buffer vao))
  (gl:bind-buffer :element-array-buffer (element-array-buffer vao))
  (gl:enable-vertex-attrib-array 0)
  (gl:vertex-attrib-pointer 0 +vertex-size+ :float nil (* +stride+ +float-size+) (cffi:make-pointer 0))
  (gl:enable-vertex-attrib-array 1)
  (gl:vertex-attrib-pointer 1 +normal-size+ :float nil (* +stride+ +float-size+) (cffi:make-pointer (* +vertex-size+ +float-size+)))
  (gl:enable-vertex-attrib-array 2)
  (gl:vertex-attrib-pointer 2 +texcoord-size+ :float nil (* +stride+ +float-size+) (cffi:make-pointer (+ (* +vertex-size+ +float-size+)
                                                                                                         (* +normal-size+ +float-size+))))
  (gl:draw-elements :triangles
                    (gl:make-null-gl-array :unsigned-short)
                    :count (size vao))
  (map nil #'gl:disable-vertex-attrib-array '(0 1 2))
  (gl:bind-texture :texture-2d 0))

;;;; OpenGL shader

(defun compile-shader-object (shader-object &optional debug)
  (compile-gl-shader (shader-source shader-object) (shader-type shader-object) debug))

(defun compile-gl-shader (source type &optional debug)
  "Compiles a GL shader string; uses error checking."
  (let ((compiled-shader (gl:create-shader type)))
    (gl:shader-source compiled-shader source)
    (gl:compile-shader compiled-shader)
    (when (and debug (not (gl:get-shader compiled-shader :compile-status)))
      (error (concatenate 'string
                          "Error in compiling shader~%"
                          (gl:get-shader-info-log compiled-shader))))
    compiled-shader))

(defun link-gl-program (compiled-shaders &optional debug)
  "Creates a GL program from a list of shaders; uses error checking."
  (let ((program (gl:create-program)))
    (unwind-protect
         (progn
           (map nil (lambda (shader) (gl:attach-shader program shader)) compiled-shaders)
           ;; todo: this line? (gl:bind-frag-data-location program color name)
           (gl:link-program program)
           (when (and debug (not (gl:get-program program :link-status)))
             (error (concatenate 'string
                                 "Error in shader program~%"
                                 (gl:get-program-info-log program)))))
      (map nil (lambda (shader) (gl:detach-shader program shader)) compiled-shaders))
    program))

(defun uniform-matrix (program matrix-name matrix)
  (gl:uniform-matrix (gl:get-uniform-location program (glsl-name matrix-name)) 4 (vector matrix) nil))

(defun uniform-vector (program vector-name vector)
  (gl:uniformfv (gl:get-uniform-location program (glsl-name vector-name)) vector))

(defun make-gl-texture (index texture)
  ;; fixme: temporary
  (let ((width (expt 2 10))
        (height (expt 2 10)))
    (gl:bind-texture :texture-2d index)
    (gl:tex-parameter :texture-2d :texture-wrap-s :repeat)
    (gl:tex-parameter :texture-2d :texture-wrap-t :repeat)
    (gl:tex-parameter :texture-2d :texture-max-anisotropy-ext *max-texture-max-anisotropy-ext*)
    (gl:tex-parameter :texture-2d :texture-min-filter :linear-mipmap-linear)
    (gl:tex-parameter :texture-2d :texture-mag-filter :linear)
    (gl:tex-image-2d :texture-2d 0 :rgba width height 0 :rgba :unsigned-byte texture)
    (gl:generate-mipmap :texture-2d)
    (gl:bind-texture :texture-2d 0)))
